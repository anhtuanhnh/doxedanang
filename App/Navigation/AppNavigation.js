import React from 'react'

import { StackNavigator, TabNavigator, TabBarBottom } from 'react-navigation'
import Icons from '../Components/AppIcons'
import I18n from '../I18n'
import { Colors } from '../assets/Themes'

import LaunchScreen from '../Containers/LaunchScreen'
import ParkingsLocationsScreen from '../Containers/Parkings/ParkingsLocationsScreen'
import ParkingLotDetailScreen from '../Containers/Parkings/ParkingLotDetailScreen'
import ParkingCarsScreen from '../Containers/ParkingCars/ParkingCarsScreen'
import ParkingSearchScreen from '../Containers/Parkings/ParkingSearchScreen'
import NotificationsScreen from '../Containers/Notifications/NotificationsScreen'
import AccountScreen from '../Containers/Account/AccountScreen'
import PaymentScreen from '../Containers/Account/PaymentScreen'
import LoginScreen from '../Containers/Auth/LoginScreen'
import RegisterScreen from '../Containers/Auth/RegisterScreen'
import ForgetPasswordScreen from '../Containers/Auth/ForgetPasswordScreen'
import ForgetPasswordFinishScreen from '../Containers/Auth/ForgetPasswordFinishScreen'
import ActiveAccountScreen from '../Containers/Auth/ActiveAccountScreen'
import ChangePasswordScreen from '../Containers/Auth/ChangePasswordScreen'
// import TabBarBottom from '../Components/TabBarBottom'
import HistoryScreen from '../Containers/History/HistoryScreen'
import HelpScreen from '../Containers/Help/HelpScreen'
import TermsAndPoliciesScreen from '../Containers/TermsAndPolicies/TermsAndPoliciesScreen'
import ProfileScreen from '../Containers/Profile/ProfileScreen'
import WebViewScreen from '../Containers/Account/WebViewScreen'
import {headerDefault} from './ConfigNavigation'

const AppStack = TabNavigator(
  {
    ParkingsLocations: { screen: ParkingsLocationsScreen },
    ParkingCars: { screen: ParkingCarsScreen },
    Notifications: { screen: NotificationsScreen },
    Account: { screen: AccountScreen }
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state
        let iconName
        if (routeName === 'ParkingsLocations') {
          iconName = `${focused ? 'bai-do-ac' : 'bai-do-df'}`
        } else if (routeName === 'ParkingCars') {
          iconName = `${focused ? 'dang-do-ac' : 'dang-do-df'}`
        } else if (routeName === 'Notifications') {
          iconName = `${focused ? 'thong-bao-ac' : 'thong-bao-df'}`
        } else if (routeName === 'Account') {
          iconName = `${focused ? 'acc-ac' : 'acc-df'}`
        }
        // You can return any component that you like here! We usually use an
        return <Icons name={iconName} size={22} color={tintColor} />
      }
    }),
    tabBarOptions: {
      activeTintColor: Colors.default,
      inactiveTintColor: Colors.charcoal,
      // showLabel: false,
      showIcon: true,
      style: {
        backgroundColor: Colors.snow,
        borderTopColor: 'transparent',
        elevation: 50
      }
    },
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    lazy: false
  }
)
const RootStack = StackNavigator({
  Home: {screen: AppStack},
  HelpScreen: {screen: HelpScreen},
  TermsAndPoliciesScreen: {screen: TermsAndPoliciesScreen},
  HistoryScreen: {screen: HistoryScreen},
  ProfileScreen: {screen: ProfileScreen},
  ParkingSearchScreen: {screen: ParkingSearchScreen},
  ParkingLotDetailScreen: {screen: ParkingLotDetailScreen},
  PaymentScreen: {screen: PaymentScreen},
  WebViewScreen: {screen: WebViewScreen},
  ChangePasswordScreen: {screen: ChangePasswordScreen},
  ActiveAccountScreen: {screen: ActiveAccountScreen},
  LoginScreen: {screen: LoginScreen},
  RegisterScreen: {screen: RegisterScreen},
  ForgetPasswordScreen: {screen: ForgetPasswordScreen},
  ForgetPasswordFinishScreen: {screen: ForgetPasswordFinishScreen}
}, {
  // Default config for all screens
  initialRouteName: 'Home',
  headerMode: 'float',
  navigationOptions: {
    ...headerDefault
  }
})

export default RootStack
