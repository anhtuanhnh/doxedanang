import { Colors } from '../assets/Themes'
import styles from './Styles/NavigationStyles'

const configStackNavigator = (initialRouteName) => ({
  headerMode: 'none',
  initialRouteName,
  navigationOptions: {
  },

  transitionConfig: () => ({
    screenInterpolator: sceneProps => {
      const { layout, position, scene } = sceneProps
      const { index } = scene
      const width = layout.initWidth

      return {
        transform: [{
          translateX: position.interpolate({
            inputRange: [index - 1, index, index + 1],
            outputRange: [width, 0, -width]
          })
        }]
      }
    }
  })
})
const headerDefault = {
  headerStyle: styles.header,
  headerTintColor: Colors.snow,
  headerTitleStyle: {
    fontWeight: 'bold'
  }
}
export {
    configStackNavigator,
  headerDefault
}
