import { takeLatest, takeEvery, all } from 'redux-saga/effects'
import API from '../Services/Api'
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { ParkingLocationsTypes } from '../Containers/Parkings/ParkingLocationsRedux'
import { AuthTypes } from '../Containers/Auth/AuthRedux'
import { ReloginTypes } from '../Redux/ReloginRedux'
import { NotificationTypes } from '../Containers/Notifications/NotificationsRedux'
import { ParkingCarsTypes } from '../Containers/ParkingCars/ParkingCarsRedux'
import { AccountTypes } from '../Containers/Account/AccountRedux'
import { HistoryTypes } from '../Containers/History/HistoryRedux'

/* ------------- Sagas ------------- */

import { startup } from './StartupSagas'
import { getParkingLocations, searchParkingLocations, getParkingDetail, checkIn } from '../Containers/Parkings/ParkingLocationsSagas'
import { getNotifications } from '../Containers/Notifications/NotigicationsSagas'
import { getPaymentTransactions } from '../Containers/History/HistorySagas'
import { getMyCheckIns, extendCheckIn, checkOut } from '../Containers/ParkingCars/ParkingCarsSagas'
import { getRelogin } from './ReloginSagas'
import * as auth from '../Containers/Auth/AuthSagas'
import * as account from '../Containers/Account/AccountSagas'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : API.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),

    // some sagas receive extra parameters in addition to an action
    takeEvery(ParkingLocationsTypes.PARKING_LOCATIONS_REQUEST, getParkingLocations, api),
    takeEvery(ParkingLocationsTypes.SEARCH_LOCATIONS_REQUEST, searchParkingLocations, api),
    takeEvery(ParkingLocationsTypes.GET_PARKING_DETAIL_REQUEST, getParkingDetail, api),
    takeEvery(ParkingLocationsTypes.CHECK_IN_REQUEST, checkIn, api),
    takeEvery(ParkingCarsTypes.PARKING_CARS_REQUEST, getMyCheckIns, api),
    takeEvery(ParkingCarsTypes.EXTEND_CHECK_IN_REQUEST, extendCheckIn, api),
    takeEvery(ParkingCarsTypes.CHECK_OUT_REQUEST, checkOut, api),
    takeEvery(NotificationTypes.GET_NOTIFICATION_REQUEST, getNotifications, api),
    takeEvery(HistoryTypes.REQUEST, getPaymentTransactions, api),
    takeEvery(AccountTypes.GET_ACCOUNT_REQUEST, account.getAccount, api),
    takeEvery(AccountTypes.GET_WALLET_REQUEST, account.getMyWallet, api),
    takeEvery(AccountTypes.UPDATE_PROFILE_REQUEST, account.updateProfile, api),
    takeEvery(AuthTypes.LOGIN_REQUEST, auth.loginUser, api),
    takeEvery(AuthTypes.REGISTER_REQUEST, auth.registerUser, api),
    takeEvery(AuthTypes.CHANGE_PASSWORD_REQUEST, auth.changePasswordUser, api),
    takeLatest(ReloginTypes.RELOGIN_REQUEST, getRelogin, api)

  ])
}
