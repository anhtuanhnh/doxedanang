import { call, put, select, all } from 'redux-saga/effects'
import AuthActions from '../Containers/Auth/AuthRedux'
import { NavigationActions } from 'react-navigation'
import AccountActions from '../Containers/Account/AccountRedux'
import NotificationActions from '../Containers/Notifications/NotificationsRedux'
import ParkingCarsActions from '../Containers/ParkingCars/ParkingCarsRedux'
import ParkingLocationsActions from '../Containers/Parkings/ParkingLocationsRedux'
export const selectAuthToken = (state) => state.auth.token

export const getRelogin = function * getRelogin (api) {
  try {
    const token = yield select(selectAuthToken)
    console.log(token)
    if (token) {
      yield call(api.setAuthorization, token)
      yield put(AuthActions.loginSuccess(token))
      yield all([
        put(AccountActions.getAccountRequest()),
        put(AccountActions.getWalletRequest()),
        put(ParkingCarsActions.parkingCarsRequest()),
        put(ParkingLocationsActions.getCarsRequest()),
        put(NotificationActions.getNotificationRequest(0))
      ])
    }
  } catch (e) {
    console.tron.display({
      name: 'getRelogin saga errorr',
      preview: 'expand',
      value: {
        message: e.message,
        e: e
      }
    })
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'ParkingsLocationsScreen' })
      ]
    })

    yield put(resetAction)
  }
}
