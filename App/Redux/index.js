import { combineReducers } from 'redux'
import { persistReducer } from 'redux-persist'
import configureStore from './CreateStore'
import rootSaga from '../Sagas/'
import ReduxPersist from '../Config/ReduxPersist'

/* ------------- Assemble The Reducers ------------- */
export const reducers = combineReducers({
  nav: require('./NavigationRedux').reducer,
  auth: require('../Containers/Auth/AuthRedux').reducer,
  relogin: require('./ReloginRedux').reducer,
  parking: require('../Containers/Parkings/ParkingLocationsRedux').reducer,
  parkingCars: require('../Containers/ParkingCars/ParkingCarsRedux').reducer,
  notifications: require('../Containers/Notifications/NotificationsRedux').reducer,
  history: require('../Containers/History/HistoryRedux').reducer,
  account: require('../Containers/Account/AccountRedux').reducer
})

export default () => {
  let finalReducers = reducers
  // If rehydration is on use persistReducer otherwise default combineReducers
  if (ReduxPersist.active) {
    const persistConfig = ReduxPersist.storeConfig
    finalReducers = persistReducer(persistConfig, reducers)
  }

  let { store, sagasManager, sagaMiddleware } = configureStore(finalReducers, rootSaga)

  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('../Redux/index').reducers
      store.replaceReducer(nextRootReducer)

      const newYieldedSagas = require('../Sagas').default
      sagasManager.cancel()
      sagasManager.done.then(() => {
        sagasManager = sagaMiddleware.run(newYieldedSagas)
      })
    })
  }

  return store
}
