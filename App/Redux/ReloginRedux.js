import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  reloginRequest: [],
  reloginSuccess: [],
  reloginFailure: null
})

export const ReloginTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Selectors ------------- */

export const ReloginSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

// request the data from an api
export const reloginRequest = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const reloginSuccess = (state, action) => {
  return state.merge({ fetching: false, error: null })
}

// Something went wrong somewhere.
export const reloginFailure = state =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.RELOGIN_REQUEST]: reloginRequest,
  [Types.RELOGIN_SUCCESS]: reloginSuccess,
  [Types.RELOGIN_FAILURE]: reloginFailure
})
