import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import icoMoonConfig from '../Fixtures/selection'
const Icons = createIconSetFromIcoMoon(icoMoonConfig)
export default Icons
// export const listingIcon = () => <Icon name='listing' size={22} color='#F2BE37' />
