import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import SpinnerScreen from '../Containers/components/SpinnerScreen'
import Network from './Network'
import { Metrics } from '../assets/Themes'

class BaseLayout extends Component {
  render () {
    const {
      containerStyle,
      fullLoading = false
    } = this.props
    return (
      <View style={{ backgroundColor: 'white', flex: 1 }}>
        <Network />
        <KeyboardAwareScrollView style={[styles.mainContainer, containerStyle]}>
          {this.props.children}
        </KeyboardAwareScrollView>
        {fullLoading && (
          <View style={styles.loader}>
            <SpinnerScreen size='large' type='dark' />
          </View>
        )}
      </View>
    )
  }
}

export default BaseLayout

const styles = StyleSheet.create({
  loader: {
    position: 'absolute',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    height: '100%',
    left: 0,
    top: 0,
    justifyContent: 'center',
    width: '100%'
  },
  mainContainer: {
    flex: 1,
    minHeight: Metrics.screenHeight - 80,
    backgroundColor: 'transparent',
    padding: 15
  }
})
