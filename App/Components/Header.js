import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Text, StatusBar, TouchableOpacity, View, Image, TextInput } from 'react-native'
// import LinearGradient from 'react-native-linear-gradient'
// import { SvgFilterButton, SvgCancelButton, SvgSearch, SvgBackButton, SvgClose} from './SvgComponent'
import { Images, Metrics, Colors } from '../assets/Themes'
import styles from '../assets/Styles/ComponentStyle'
import Icons from './AppIcons'
// import {translate} from '../I18n1/translate';

export default class Header extends Component {
  renderIcon = (type) => {
    switch (type) {
      case 'clean' : return <Icons name={'back'} size={22} color={Colors.default} />
      case 'back' : return <Icons name={'back'} size={14} color={Colors.snow} />
      case 'reload' : return <Icons name={'refresh'} size={20} color={Colors.snow} />
      case 'search' : return <Icons name={'search-xanh'} size={20} color={Colors.snow} />
    }
  }
  render () {
    const {
      header = {
        headerType: 'normal'
      }
    } = this.props
    return (
      <View style={[styles.boxHeader, header.headerType === 'search' ? {height: 76} : null]}>
        <StatusBar
          backgroundColor='transparent'
          barStyle='light-content'
          translucent
        />
        <View
          start={{x: 0.0, y: 0.25}} end={{x: 0.5, y: 1.0}}
          locations={[0, 1]}
          colors={header.headerType === 'clean' ? ['transparent', 'transparent'] : ['#00D034', '#00B851']}
          style={styles.linearGradient}>
          {
            (header.headerType === 'normal' || header.headerType === 'clean')
            ? <View style={styles.boxTitleHeader}>
              {
                header.buttonLeftType &&
                <TouchableOpacity
                  onPress={() => header.buttonLeftOnPress()}
                  style={styles.boxButton}
                >
                  {this.renderIcon(header.buttonLeftType)}
                </TouchableOpacity>
              }
              {
                header.title &&
                <Text style={styles.textTitle}>
                    {translate(header.title)}
                </Text>
              }
              {
                  header.buttonRightType &&
                  <TouchableOpacity
                    onPress={() => header.buttonRightOnPress()}
                    style={styles.boxButtonRight}
                  >
                    {this.renderIcon(header.buttonRightType)}
                  </TouchableOpacity>
                }
            </View> : null
          }
          {
            header.headerType === 'search' &&
            <View style={styles.boxSearchHeader}>
              <TextInput
                style={styles.inputSearch}
                underlineColorAndroid='transparent'
              />
              <TouchableOpacity style={styles.boxIconLeft} onPress={header.buttonSearchOnPress}>
                {/* <SvgSearch /> */}
              </TouchableOpacity>
              <TouchableOpacity style={styles.boxIconRight}>
                {/* <SvgCancelButton /> */}
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.boxFilterButton}
                onPress={header.buttonRightOnPress}
              >
                {/* <SvgFilterButton /> */}
              </TouchableOpacity>
            </View>
          }
        </View>
      </View>
    )
  }
}

Header.propTypes = {
  header: PropTypes.object
}
