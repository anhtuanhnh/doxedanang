import React, { Component } from 'react'
import {View, Text, TextInput, StyleSheet} from 'react-native'
import Header from './Header'
import Network from './Network'

class MapLayout extends Component {
  render () {
    const {
        } = this.props
    return (
      <View style={styles.container}>
        <Network />
        {this.props.children}
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: { ...StyleSheet.absoluteFillObject }
})
export default MapLayout
