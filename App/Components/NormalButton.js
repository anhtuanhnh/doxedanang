import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, Text } from 'react-native'
import styles from '../assets/Styles/ComponentStyle'
// import Translate from "../I18n1/translate";

export default class NormalButton extends Component {
  constructor (props) {
    super(props)
    this.state = {
      disable: props.disable
    }
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.disable !== this.props.disable) {
      this.setState({disable: nextProps.disable})
    }
  }
  render () {
    const {
        onPress,
        text
    } = this.props
    return (
      <TouchableOpacity
        activeOpacity={1}
        disabled={this.state.disable}
        style={[styles.button, this.props.styles, this.state.disable && { backgroundColor: '#c3c5c8'}]}
        onPress={onPress}
      >
        <Text style={styles.buttonText}>{text.toUpperCase()}</Text>
      </TouchableOpacity>
    )
  }
}

NormalButton.propTypes = {
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func,
  styles: PropTypes.object,
  disable: PropTypes.bool
}
NormalButton.defaultProps = {
  disable: false,
  styles: {}
}
