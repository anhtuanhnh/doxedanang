import {Keyboard, Animated} from 'react-native'
import React from 'react'
import { createBottomTabNavigator, BottomTabBar } from 'react-navigation-tabs'

const TAB_BAR_OFFSET = -60

class TabBarBottom extends React.PureComponent {
  constructor (props) {
    super(props)

    this.keyboardWillShow = this.keyboardWillShow.bind(this)
    this.keyboardWillHide = this.keyboardWillHide.bind(this)

    this.state = {
      isVisible: true,
      offset: new Animated.Value(0)

    }
  }
  componentWillReceiveProps (props) {
    const oldState = this.props.navigation.state
    const oldRoute = oldState.routes[oldState.index]
    const oldParams = oldRoute.routes[oldRoute.index].params ? oldRoute.routes[oldRoute.index].params : null
    const wasVisible = !oldParams || oldParams.visible

    const newState = props.navigation.state
    if (newState) {
      const newRoute = newState.routes[newState.index]
      const newParams = newRoute.routes[newRoute.index].params ? newRoute.routes[newRoute.index].params : null
      const isVisible = !newParams || newParams.visible

      if (wasVisible && !isVisible) {
        Animated.timing(this.state.offset, { toValue: TAB_BAR_OFFSET, duration: 0 }).start()
      } else if (isVisible && !wasVisible) {
        Animated.timing(this.state.offset, { toValue: 0, duration: 0 }).start()
      }
            // console.log(oldRoute.index, newRoute.index)
    }
  }

  componentWillMount () {
    this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardWillShow)
    this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide)
  }

  componentWillUnmount () {
    this.keyboardWillShowSub.remove()
    this.keyboardWillHideSub.remove()
  }

  keyboardWillShow = (event) => {
    this.setState({
      isVisible: false
    })
  }

  keyboardWillHide = (event) => {
    this.setState({
      isVisible: true
    })
  }

  render () {
    return this.state.isVisible
            ? <BottomTabBar {...this.props} style={[styles.container, { bottom: this.state.offset }]} />
            : null
  }
}
const styles = {
  container: {
    overflow: 'hidden',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'white',
    elevation: 8
  }
}
export default TabBarBottom
