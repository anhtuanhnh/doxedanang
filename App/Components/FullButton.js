import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, Text, View } from 'react-native'
import styles from '../assets/Styles'

export default class FullButton extends Component {
  render () {
    return (
      <View style={styles.pldButton}>
        <TouchableOpacity
          activeOpacity={1}
          disable={this.props.disable}
          onPress={this.props.onPress}
          style={[styles.pldButtonItem, styles.pldButtonSpecial, this.props.styles, this.props.disable && { backgroundColor: '#c3c5c8'}]}>
          <Text style={styles.pldButtonSpecialText}>
            {this.props.text.toUpperCase()}
          </Text>
        </TouchableOpacity>
      </View>
    )
  }
}

FullButton.propTypes = {
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func,
  styles: PropTypes.object,
  disable: PropTypes.bool
}
FullButton.defaultProps = {
  disable: false,
  styles: {}
}
