import React, { Component } from 'react'
import { View, ActivityIndicator, StatusBar, Alert } from 'react-native'
export default () => {
  return (
    <View style={[{ flex: 1, alignItems: 'center', justifyContent: 'center' }]}>
      <ActivityIndicator />
      <StatusBar barStyle='default' />
    </View>
  )
}
