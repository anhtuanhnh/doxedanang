import Toast from 'react-native-root-toast'

export default (content, timer = 3000, cb) => {
  let toast = Toast.show(content, {
    duration: 0,
    position: -100,
    shadow: true,
    animation: true,
    hideOnPress: true,
    delay: 0,
    backgroundColor: '#3F8BF4',
    onShow: () => {
            // calls on toast\`s appear animation start
    },
    onShown: () => {
      if (cb)cb()
            // calls on toast\`s appear animation end.
    },
    onHide: () => {
            // calls on toast\`s hide animation start.
    },
    onHidden: () => {
            // calls on toast\`s hide animation end.
    }
  })

  setTimeout(function () {
    Toast.hide(toast)
  }, timer)
}
