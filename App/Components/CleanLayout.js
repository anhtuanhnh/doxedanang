import React, { Component } from 'react'
import { View} from 'react-native'
import styles from '../assets/Styles/index'
import SpinnerScreen from '../Containers/components/SpinnerScreen'
import Network from './Network'

class CleanLayout extends Component {
  render () {
    const {
      fullLoading = false,
      quickLoading = false
        } = this.props
    return (
      <View style={styles.container}>
        <Network />
        {this.props.children}
        {fullLoading && (
          <View style={styles.loader}>
            <SpinnerScreen size='large' type='dark' />
          </View>
        )}
        {quickLoading && (
          <View style={styles.cleanLoader}>
            <SpinnerScreen size='large' type='selector' />
          </View>
        )}
      </View>
    )
  }
}

export default CleanLayout
