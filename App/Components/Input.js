import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Text, TextInput, TouchableOpacity, Slider, ScrollView, Platform } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { createAnimatableComponent, View } from 'react-native-animatable'

import { Colors, Images } from '../assets/Themes'
import styles from '../assets/Styles/ComponentStyle'
import Icons from './AppIcons'

export default class Input extends Component {
  constructor (props) {
    super(props)
    this.state = {
      secureTextEntry: props.secureTextEntry || false,
      textInputValue: '',
      errors: props.errors || [],
      autoFocus: props.autoFocus || false,
      showOptions: false
    }
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.errors !== this.props.errors) {
      this.setState({errors: nextProps.errors})
    }
    if (nextProps.autoFocus !== this.props.autoFocus) {
      this.setState({autoFocus: nextProps.autoFocus})
    }
  }
  render () {
    const {
      // general
      value,
      keyboardType,
      refNode,
      onSubmitEditing,
      onEndEditing,
      onFocus,
      maxLength,
      onlyRead,
      style,
      onChangeText,
      returnKeyType,
      autoCapitalize,
      autoCorrect,
      // base input
      label,
      press,
      rightPress,
      baseInput,
      editable,
      selectInput,
      iconInput,
      iconInputRight,
      styleLabel,
      styleInput,
      iconLeft = '#A0ABBF',
      placeHolder = '',
      // select input
      options,
      onStartShouldSetResponderCapture,
      // slider input
      valueSlider,
      slideInput,
      maximumValue,
      step,
      styleSlider
    } = this.props
    const {secureTextEntry, errors, autoFocus, showOptions} = this.state
    return (
      <View style={style}>
        {label && <Text style={styleLabel}>
          {label}
        </Text>}
        {slideInput && <Text style={styles.labelSlider}>{valueSlider} giờ</Text>}

        {
          baseInput && <View style={[styles.boxBaseInput]}>
            {iconInput && <View style={styles.boxIconInput}><Icons name={iconInput} size={18} color={iconLeft} /></View>}
            <TextInput
              style={[styleInput, iconInput && { paddingLeft: 50}, iconInputRight && styles.hasIconRight, editable === false && { backgroundColor: '#F0F2F6', color: '#2C2C2C'}]}
              value={value}
              editable={editable}
              autoFocus={autoFocus}
              ref={refNode}
              placeholder={placeHolder}
              keyboardType={keyboardType}
              secureTextEntry={secureTextEntry}
              underlineColorAndroid={'transparent'}
              onChangeText={text => press(text)}
              onEndEditing={onEndEditing}
              onSubmitEditing={onSubmitEditing}
              onFocus={onFocus}
              maxLength={maxLength}
              returnKeyType={returnKeyType}
              autoCapitalize={autoCapitalize}
              autoCorrect={autoCorrect}
            />
            {iconInputRight && <TouchableOpacity style={styles.boxIconInputRight} onPress={() => { rightPress ? rightPress() : this.setState({secureTextEntry: !secureTextEntry}) }}><Icons name={iconInputRight} size={14} color={'#A0ABBF'} /></TouchableOpacity>}
          </View>
        }
        {
          errors && errors.map((item, index) => (
            <Text key={index} style={styles.textError}>{item}</Text>
          ))
        }
        {
          selectInput &&
          <View>
            <TextInput
              style={[styleInput, styles.selectInput]}
              value={value}
              onChangeText={text => onChangeText(text)}
              underlineColorAndroid={'transparent'}
              editable={editable}
            />
            <TouchableOpacity
              onPress={() => this.setState({showOptions: !showOptions})}
              style={styles.boxIconInputRight}
            >
              <Icons name='arrow-down' size={7} color={Colors.default} />
            </TouchableOpacity>
            {
              showOptions &&
              <View
                animation={'fadeInDown'} delay={0}
                easing='ease-in-out-back'
                collapsable={false}
                onStartShouldSetResponderCapture={onStartShouldSetResponderCapture}
                ref={'selectInput'}
              >
                <ScrollView
                  style={{ borderWidth: 1, borderColor: '#E7EAF0', borderRadius: 3, paddingBottom: 20}}>
                  {
                    options.map((item, index) => (
                      <TouchableOpacity
                        key={index}
                        onPress={() => {
                          press(item)
                          this.setState({showOptions: false})
                        }}

                        style={[
                          index % 2 && { backgroundColor: '#E7EAF0'},
                          { paddingTop: 7, paddingBottom: 7, paddingLeft: 20 }]}>
                        <Text style={[
                          item.key === value && { fontWeight: '600'}
                        ]}>
                          {item.label}
                        </Text>
                      </TouchableOpacity>
                    ))
                  }
                </ScrollView>
              </View>
            }
          </View>
        }
        {
          slideInput &&
          <View>
            <Slider
              style={styleSlider}
              maximumValue={maximumValue}
              thumbTintColor={Colors.default}
              minimumTrackTintColor={Colors.default}
              value={valueSlider}
              onSlidingComplete={(number) => press(number)}
              step={step}
            />
          </View>
        }
        {
          onlyRead &&
          <View>
            <Text>
              <Text style={styles.labelSlider}>
                {value}
              </Text>
              / 2giờ
            </Text>
          </View>
        }
      </View>
    )
  }
}
Input.propTypes = {
  // value: PropTypes.string,
  valueSlider: PropTypes.number,
  label: PropTypes.string,
  press: PropTypes.func,
  multiline: PropTypes.bool,
  baseInput: PropTypes.bool,
  editable: PropTypes.bool,
  selectInput: PropTypes.bool,
  slideInput: PropTypes.bool,
  autoFocus: PropTypes.bool,
  onlyRead: PropTypes.bool,
  secureTextEntry: PropTypes.bool,
  iconInput: PropTypes.string,
  iconInputRight: PropTypes.string,
  keyboardType: PropTypes.string,
  style: PropTypes.object,
  // styleLabel: PropTypes.object,
  // styleInput: PropTypes.object,
  // styleSlider: PropTypes.object,
  maxLength: PropTypes.number,
  step: PropTypes.number,
  maximumValue: PropTypes.number,
  options: PropTypes.array
  // ref: PropTypes.func,
}
Input.defaultProps = {
  multiline: false,
  baseInput: false,
  slideInput: false,
  editable: true,
  isFocused: false,
  selectInput: false,
  onlyRead: false,
  step: 2,
  styleLabel: {},
  styleInput: {},
  styleSlider: {},
  options: [],
  press: (e) => { console.log(e) }
}
