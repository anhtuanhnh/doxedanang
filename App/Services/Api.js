// a library to wrap and simplify api calls
import apisauce from 'apisauce'
const baseURL = 'https://dev-api.doxedanang.com/api/'
const PAYMENT_URL = 'https://dev-payment.doxedanang.com/api/'
const IMG_URL = 'https://dev-static.doxedanang.com/api/image/'

// our "constructor"
const create = () => {
  // ------
  // STEP 1
  // ------
  //
  // Create and configure an apisauce-based api object.
  //
  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache'
    },
    // 10 second timeout...
    timeout: 10000
  })

  const getParkingLocations = () => api.get('parking-locations?page=0&size=100')
  const searchParkingLocations = (query) => api.get(`_search/parking-locations?query=${query}`)
  const getParkingDetail = (parkingId) => api.get(`parking-locations/${parkingId}`)
  const getCars = () => api.get(`myRecentCheckinCars?page=0&size=5&sort=lastCheckinTime,desc`)
  const checkIn = (data) => api.post(`checkin`, data)
  const checkOut = (data) => api.post(`checkout`, data)
  // require auth
  const getMyCheckIns = () => api.get('myCheckins?page=0&size=40&sort=id,desc')
  const extendCheckIn = (data) => api.post('extendCheckin', data)
  const getNotifications = (page = 0) => api.get(`myNotifications?page=${page}&size=20&sort=id,desc`)
  const getPaymentTransactions = (page = 0) => api.get(`myPaymentTransactions?page=${page}&size=100&sort=id,desc`)
  const getAccount = () => api.get(`account`)
  const updateProfile = (data) => api.post(`account/profile`, data)
  const getMyWallet = () => api.get(`myWallet`)
  // auth
  const loginUser = (user) => api.post('authenticate', user)
  const registerUser = () => api.post('register')
  const changePasswordUser = (password) => api.post('account/change_password', password)
  const setAuthorization = (token) => api.setHeader('Authorization', 'Bearer ' + token)
  // ------
  // STEP 3
  // ------
  //
  // Return back a collection of functions that we would consider our
  // interface.  Most of the time it'll be just the list of all the
  // methods in step 2.
  //
  // Notice we're not returning back the `api` created in step 1?  That's
  // because it is scoped privately.  This is one way to create truly
  // private scoped goodies in JavaScript.
  //
  return {
    // a list of the API functions from step 2
    getParkingLocations,
    searchParkingLocations,
    getParkingDetail,
    getMyCheckIns,
    extendCheckIn,
    getNotifications,
    getPaymentTransactions,
    getAccount,
    getMyWallet,
    loginUser,
    registerUser,
    setAuthorization,
    updateProfile,
    getCars,
    checkIn,
    checkOut,
    changePasswordUser
  }
}

// let's return back our create method as the default.
export default {
  create
}
