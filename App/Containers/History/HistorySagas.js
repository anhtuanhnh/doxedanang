import { call, put } from 'redux-saga/effects'
import HistoryActions from './HistoryRedux'

export function * getPaymentTransactions (api, action) {
  // make the call to the api
  const { page } = action
  const response = yield call(api.getPaymentTransactions, page)

  if (response.ok) {
    const { data } = response
    // do data conversion here if needed
    yield put(HistoryActions.success(data))
  } else {
    yield put(HistoryActions.failure())
  }
}
