import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, Text, RefreshControl, ActivityIndicator, FlatList } from 'react-native'
import {Colors, Metrics} from '../../assets/Themes/index'
import styles from '../../assets/Styles/index'
import CleanLayout from '../../Components/CleanLayout'
import Icons from '../../Components/AppIcons'
import _ from 'lodash'
import moment from 'moment'
import 'moment/locale/vi'
import 'moment/locale/en-au'
import I18n from '../../I18n/index'
import HistoryActions from './HistoryRedux'

const fullWidth = Metrics.screenWidth

class HistoryScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      transactions: []
    }
  }
  componentDidMount () {
    this.getPaymentTransactions()
  }
  componentWillReceiveProps (nextProps) {
    console.log(nextProps)
    if (nextProps.history !== this.props.history) {
      this.handleTransactions(nextProps.history)
    }
  }
  getPaymentTransactions = () => {
    this.props.getMyPaymentTransactions(0)
  }

  onEndReached = (fetching, page, getMyPaymentTransactions) => {
    if (!fetching) {
      getMyPaymentTransactions(page)
    }
  }
  _onRefresh =() => {
    this.getPaymentTransactions(0)
  }
  handleTransactions = (data) => {
    moment.locale('vi')
    const arrays = []
    for (let i = 0; i < data.length; i++) {
      const index = _.findIndex(arrays, o => (
        o.key === moment(data[i].created).format('dddd, Do MMMM  YYYY')
      ))
      if (index === -1) {
        arrays.push({
          key: moment(data[i].created).format('dddd, Do MMMM  YYYY'),
          code: new Date(data[i].created).valueOf(),
          array: [data[i]]
        })
      } else {
        arrays[index].array.push(data[i])
      }
    }
    _.sortBy(arrays, ['code'])
    this.setState({transactions: arrays})
  }
  _renderRow =(row) => {
    return (
      <View style={[{flexDirection: 'column', flex: 1, marginLeft: 15, marginRight: 15}]}>
        <Text style={{fontSize: 13, lineHeight: 18, fontWeight: '600', marginBottom: 8}}>{row.key && row.key.toUpperCase()}</Text>
        {
          row.array.map((subItem, subIndex) => (
            <View key={`${row.key}${subIndex}`} style={[styles.rowFlexStart, { elevation: 5, height: 88, backgroundColor: 'white', borderRadius: 3, paddingVertical: 15, paddingHorizontal: 20, marginBottom: 24}]}>
              <View style={{width: 55, alignItems: 'center', justifyContent: 'center'}}>
                <View style={{backgroundColor: '#3F8BF4',
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: 50,
                  height: 40,
                  width: 40}}>
                  <Icons name={'arrow-up'} size={16} color={Colors.snow} />
                </View>
              </View>
              <View style={[styles.columnFlexStart, { width: fullWidth - 195, paddingLeft: 5 }]}>
                <Text>{this.getContent(subItem.paymentType, subItem.status)}</Text>
                <Text>{moment(subItem.created).format('HH:mm DD/MM/YYYY')}</Text>
              </View>
              <View style={[styles.columnFlexStart, {flex: 0, width: 70, alignItems: 'flex-end'}]}>
                <Text style={{fontSize: 13, fontWeight: '600', color: '#3F8BF4'}}>{this.getAmount(subItem.paymentType, subItem.status, subItem.amount)}</Text>
                <Text>{subItem.balance && subItem.balance.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</Text>
              </View>
            </View>
          ))
        }
      </View>
    )
  }
  renderSeparator = () => {
    return <View style={{height: 15}} />
  }
  getContent = (paymentType, status) => {
    if (paymentType === 1) return I18n.t('Recharge from scratch card')
    else if (paymentType === 2 && status !== 2) return I18n.t('Initialize recharging from domestic ATM card')
    else if (paymentType === 2 && status === 2) return I18n.t('Recharge from domestic ATM card')
    else if (paymentType === 3) return I18n.t('Pay parking fee')
    else if (paymentType === 4) return I18n.t('Promotion when creating a new account')
    else if (paymentType === 5 && status !== 2) return I18n.t('Initialize recharge from Visa / Master card')
    else if (paymentType === 5 && status === 2) return I18n.t('Recharge from Visa / Master card')
    else if (paymentType === 6) return I18n.t('Pay parking fee by text message to 7783')
    else if (paymentType === 7) return I18n.t('Pay parking fee renewal')
    else if (paymentType === 9) return I18n.t('Pay the violation at the box office')
    else if (paymentType === 10) return I18n.t('Pay the violation from the app')
    else if (paymentType === 11) return I18n.t('Recharge from scratch card by another')
    else if (paymentType === 12) return I18n.t('Refund the user')
    else if (paymentType === 13 && status !== 2) return I18n.t('Recharge from scratch card of telco partner - Fail')
    else if (paymentType === 13 && status === 2) return I18n.t('Recharge from scratch card of telco partner')
    else if (paymentType === 14) return I18n.t('Pay monthly ticket')
    return ''
  };

  getSubContent = (transactionID, created, update, paymentType, status) => {
    let _return = ``

    if (paymentType === 2 && status === 2) {
      _return += `${moment(created).format('LT')} - ${moment(update).format('LT')}`
    } else if (paymentType === 5 && status === 2)	{
      _return += `${moment(created).format('LT')} - ${moment(update).format('LT')}`
    } else	{
      _return += moment(created).format('LT')
    }

    return _return
  };

  getAmount = (paymentType, status, amount = 0) => {
    amount = amount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    switch (paymentType) {
      case 1 : return `+${amount}`
      case 2 && status === 2 : return `+${amount}`
      case 3: return `-${amount}`
      case 4: return `+${amount}`
      case 5 && status === 2: return `+${amount}`
      case 6: return amount
      case 7: return `-${amount}`
      case 9: return `-${amount}`
      case 10: return `-${amount}`
      case 11: return `+${amount}`
      case 12: return `+${amount}`
      case 13 && status === 2: return `+${amount}`
      case 14: return `-${amount}`
      default: return amount
    }
  };
  render () {
    const { transactions } = this.state
    const { wallet, fetching, page, getMyPaymentTransactions } = this.props
    return (
      <CleanLayout>
        <View style={[styles.rowSpaceAround, {flex: 0, padding: 13, backgroundColor: 'white'}]}>
          <Text style={{flex: 1, fontSize: 13, paddingLeft: 2, lineHeight: 18}}>{I18n.t('Balance')}</Text>
          <Text style={{flex: 1, paddingRight: 2, textAlign: 'right', fontSize: 15, fontWeight: '600', color: '#2C2C2C'}}>{`${wallet.amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,')} đ`}</Text>
        </View>
        {
          transactions.length > 0
            ? <FlatList
              data={transactions}
              style={{ marginTop: 15}}
              refreshControl={
                <RefreshControl
                  refreshing={false}
                  onRefresh={() => this._onRefresh()}
                />
              }
              // showsVerticalScrollIndicator={false}
              keyExtractor={(item) => item.code.toString()}
              renderItem={row => this._renderRow(row.item)}
              ItemSeparatorComponent={this.renderSeparator}
              onEndReached={() => { this.onEndReached(fetching, page, getMyPaymentTransactions) }}
            />
            : <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <ActivityIndicator />
            </View>

        }

      </CleanLayout>
    )
  }
}

const mapStateToProps = (state) => ({
  fetching: state.history.fetching,
  history: state.history.list,
  page: state.history.page,
  wallet: state.account.wallet

})
const mapDispatchToProps = (dispatch) => ({
  getMyPaymentTransactions: (page) => dispatch(HistoryActions.request(page))
})
HistoryScreen.navigationOptions = {
  title: I18n.t('History')
}
export default connect(mapStateToProps, mapDispatchToProps)(HistoryScreen)
// Lịch sử giao dịch
