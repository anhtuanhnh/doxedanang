import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  request: ['page'],
  success: ['data'],
  failure: null
})

export const HistoryTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  list: [],
  fetching: null,
  error: null,
  page: 0
})

/* ------------- Selectors ------------- */

// export const GithubSelectors = {
//   selectAvatar: state => state.github.avatar
// }

/* ------------- Reducers ------------- */

// request get notifications
export const request = (state) => state.merge({ fetching: true })

// successful get notifications
export const success = (state, action) => {
  const { data } = action
  return state.merge({ fetching: false, error: null, list: [...state.list, ...data], page: state.page + 1 })
}

// failed to get notifications
export const failure = (state) =>
  state.merge({ fetching: false, error: true, list: [], page: 0 })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.REQUEST]: request,
  [Types.SUCCESS]: success,
  [Types.FAILURE]: failure
})
