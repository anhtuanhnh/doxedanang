import React, { Component } from 'react'
import { View, Text, Image, ScrollView, TouchableOpacity, Platform, Alert } from 'react-native'
import { connect } from 'react-redux'
import { StackActions, NavigationActions } from 'react-navigation'

// libraries
// components
import CleanLayout from '../../Components/BaseLayout'
import Icons from '../../Components/AppIcons'
import Input from '../../Components/Input'
import toast from '../../Components/Toast'
// actions
import ParkingActions from './ParkingLocationsRedux'
// styles
import img from '../../assets/Images/parkingLocation.jpg'
import styles from '../../assets/Styles/index'
import I18n from '../../I18n/index'
import { Metrics } from '../../assets/Themes/index'
const carTypes = [{
  label: I18n.t('Type 1: Passenger car under 16 seats, truck under 2.5 tons'),
  key: 1
},
{
  label: I18n.t('Type 2: Passenger car 16 - 30 seats, truck 2.4 - 3.5 tons'),
  key: 2
},
{
  label: I18n.t('Type 3: Passenger car over 30 seats, truck over 3.5 tons'),
  key: 3
}]
class ParkingLotDetailScreen extends Component {
  state = {
    carNumber: '43K21543',
    carType: 'Loại 1: Xe khách dưới 16 chỗ, xe tải ...',
    duration: 2,
    listCars: [],
    checkingPriceType: 1,
    enableScrollViewScroll: true
  }
  componentWillMount () {
    this.getParkingDetail()
  }
  componentWillReceiveProps (nextProps, nextState) {
    if (nextProps.cars !== this.props.cars) {
      this.handleListCar(nextProps.cars)
    }
  }
  handleListCar = async (cars) => {
    const listCars = []
    if (!cars) return false
    await cars.forEach(value => {
      listCars.push({ label: value.number, key: value.number })
    })
    return this.setState({ listCars, carNumber: listCars[0].key, checkingPriceType: cars[0].carType })
  }
  getParkingDetail = () => {
    const { parkingId } = this.props.navigation.state.params
    this.props.getParkingDetail(parkingId)
  }
  handleTotalPrice = (checkingPriceType, duration) => {
    const { parkingDetail } = this.props
    switch (checkingPriceType) {
      case 1: return duration * parkingDetail.priceByHourOne
      case 2: return duration * parkingDetail.priceByHourTwo
      case 3: return duration * parkingDetail.priceByHourThree
    }
  }
  checkIn = () => {
    const {checkingPriceType, duration, carNumber} = this.state
    const {parkingDetail} = this.props
    const data = {
      checkingType: 2,
      parkingLocation: {
        parkingCode: parkingDetail.parkingCode
      },
      checkingPriceType: checkingPriceType,
      duration: duration,
      carNumber: carNumber,
      checkinAppType: Platform.OS
    }
    Alert.alert(
      I18n.t('SmartParking alerts'),
      I18n.t(`Do you park ${duration} hours for ${carNumber} at ${parkingDetail.parkingCode} parking with price ${this.handleTotalPrice(checkingPriceType, duration).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')} VND?`),
      [
          {text: I18n.t('Cancel'), style: 'cancel'},
        {
          text: I18n.t('OK'),
          onPress: () => {
            this.props.checkIn(data)
          }
        }
      ],
        {cancelable: false}
    )
  }
  onFocusDropDown = () => {
    this.setState({ enableScrollViewScroll: false })
  }
  render () {
    const { carNumber, carType, checkingPriceType, duration, listCars } = this.state
    const { parkingDetail, fetching, fetchingCheckIn } = this.props
    const maximumValue = 6
    return (
      <CleanLayout
        fullLoading={fetching || fetchingCheckIn}
        containerStyle={{ padding: 0, height: Metrics.screenHeight - 80 }}
      >
        {
          parkingDetail && <View
            style={styles.pldMainContainer}
            onStartShouldSetResponderCapture={() => { this.setState({ enableScrollViewScroll: true }) }}>
            <View style={styles.parkingLotDetailBanner}>
              <Image style={styles.parkingLotDetailImage} source={img} />
            </View>
            <View style={styles.parkingLotDetailContent}>
              <View style={[styles.pldItem, styles.pb20]}>
                <Icons name={'dia-diem-bai-do'} style={styles.pldItemIcon} size={11} color={'#4A4A4A'} />
                <Text style={styles.pldItemText}>
                  <Text style={styles.textBold}>{I18n.t('Address: ')}</Text>
                  {parkingDetail.address}
                </Text>
              </View>
              <View style={[styles.pldItem, styles.pb20]}>
                <Icons name={'thoi-gian'} style={styles.pldItemIcon} size={11} color={'#4A4A4A'} />
                <Text style={styles.pldItemText}>
                  <Text style={styles.textBold}>{I18n.t('Open time: ')}</Text>
                  {parkingDetail.openHour}
                </Text>
              </View>
              <View style={styles.pldItem}>
                <Icons name={'vi-tri-con-trong'} style={styles.pldItemIcon} size={11} color={'#4A4A4A'} />
                <Text style={styles.pldItemText}>
                  <Text style={styles.textBold}>{I18n.t('Available parks: ')}</Text>
                  {I18n.t(`${parkingDetail.availableParks} Available parks`)}
                </Text>
              </View>
            </View>
            <View style={styles.pldForm}>
              <Input
                label={I18n.t('Car number:').toUpperCase()}
                styleLabel={styles.pldFormLabel}
                styleInput={styles.formInputSelect}
                value={carNumber}
                options={listCars}
                selectInput
                ref={'myList'}
                onChangeText={(value) => { this.setState({ carNumber: value }) }}
                onStartShouldSetResponderCapture={this.onFocusDropDown}
                press={(res) => { this.setState({ carNumber: res.key }) }} />
              <Input
                label={I18n.t('Car type:').toUpperCase()}
                style={{ marginTop: 20 }}
                styleLabel={styles.pldFormLabel}
                styleInput={styles.formInputSelect}
                options={carTypes}
                value={carType}
                selectInput
                editable={false}
                onStartShouldSetResponderCapture={this.onFocusDropDown}
                press={(res) => { this.setState({ checkingPriceType: res.key, carType: res.label }) }} />
              <View style={{
                flexDirection: 'row',
                justifyContent: 'space-around'
              }}>
                <Input label={I18n.t('Parking time:').toUpperCase()} style={{ marginTop: 20, width: '50%' }} styleLabel={styles.pldFormLabel} styleSlider={styles.pldFormSlider} valueSlider={duration} maximumValue={maximumValue} slideInput press={(duration) => { this.setState({ duration }) }} />
                <Input label={I18n.t('Parking fee:').toUpperCase()} style={{ marginTop: 20, alignItems: 'flex-end', width: '50%' }} value={parkingDetail.price} onlyRead styleLabel={styles.pldFormLabel} />
              </View>
            </View>
          </View>
        }
        {
          parkingDetail &&
          <View style={styles.pldButton}>
            <View style={styles.pldButtonItem}>
              <Text style={styles.pldItemText}>
                {I18n.t('Fee:')}
                <Text style={[styles.textBold, { color: 'red' }]}>
                  {`${this.handleTotalPrice(checkingPriceType, duration).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')} đ`}
                </Text>
              </Text>
            </View>
            <TouchableOpacity
              style={[styles.pldButtonItem, styles.pldButtonSpecial]}
              onPress={this.checkIn}>
              <Text style={styles.pldButtonSpecialText}>
                {I18n.t('Check in now').toUpperCase()}
              </Text>
            </TouchableOpacity>
          </View>
        }
      </CleanLayout>
    )
  }
}
const mapStateToProps = (state) => ({
  parkingDetail: state.parking.parkingDetail,
  fetchingCheckIn: state.parking.fetchingCheckIn,
  cars: state.parking.cars,
  fetching: state.parking.fetchingDetail
})
const mapDispatchToProps = (dispatch) => ({
  getParkingDetail: (parkingId) => dispatch(ParkingActions.getParkingDetailRequest(parkingId)),
  checkIn: (data) => dispatch(ParkingActions.checkInRequest(data))
})
ParkingLotDetailScreen.navigationOptions = ({ navigation }) => {
  const { name } = navigation.state.params
  return ({
    title: `${name}`
  })
}
export default connect(mapStateToProps, mapDispatchToProps)(ParkingLotDetailScreen)
