import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  parkingLocationsRequest: null,
  parkingLocationsSuccess: ['data'],
  parkingLocationsFailure: null,

  searchLocationsRequest: ['query'],
  searchLocationsSuccess: ['data'],
  searchLocationsFailure: null,

  getParkingDetailRequest: ['parkingId'],
  getParkingDetailSuccess: ['data'],
  getParkingDetailFailure: null,

  getCarsRequest: null,
  getCarsSuccess: ['data'],
  getCarsFailure: null,

  checkInRequest: ['data'],
  checkInSuccess: null,
  checkInFailure: null
})

export const ParkingLocationsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  parkingLocations: null,
  fetching: null,
  fetchingDetail: null,
  fetchingCheckIn: null,
  fetchingSearch: null,
  error: null,
  searchLocations: null,
  parkingDetail: null,
  cars: null,
  checkInSuccess: null
})

/* ------------- Selectors ------------- */

// export const GithubSelectors = {
//   selectAvatar: state => state.github.avatar
// }

/* ------------- Reducers ------------- */

export const parkingLocationsRequest = (state) =>
  state.merge({ fetching: true })

export const parkingLocationsSuccess = (state, action) => {
  const { data } = action
  return state.merge({ fetching: false, error: null, parkingLocations: data })
}

export const parkingLocationsFailure = (state) =>
  state.merge({ fetching: false, error: true })

export const searchLocationsRequest = (state) =>
  state.merge({ fetchingSearch: true })

export const searchLocationsSuccess = (state, action) => {
  const { data } = action
  return state.merge({ fetchingSearch: false, error: null, searchLocations: data })
}

export const searchLocationsFailure = (state) =>
  state.merge({ fetchingSearch: false, error: true })

export const getParkingDetailRequest = (state) =>
  state.merge({ fetchingDetail: true })

export const getParkingDetailSuccess = (state, action) => {
  const { data, dataCar } = action
  return state.merge({ fetchingDetail: false, error: null, parkingDetail: data, cars: dataCar })
}

export const getCarsRequest = (state) => {
  return state.merge({ fetchingDetail: true })
}
export const getCarsSuccess = (state, { data }) => {
  return state.merge({ fetchingDetail: false, error: null, cars: data })
}

export const getCarsFailure = (state) => {
  return state.merge({ fetchingDetail: false, error: true })
}

export const getParkingDetailFailure = (state) =>
  state.merge({ fetchingDetail: false, error: true })

export const checkInRequest = (state) =>
  state.merge({ fetchingCheckIn: true, checkInSuccess: null })

export const checkInSuccess = (state, action) =>
  state.merge({ fetchingCheckIn: false, error: null, checkInSuccess: true })

export const checkInFailure = (state) =>
  state.merge({ fetchingCheckIn: false, error: true })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PARKING_LOCATIONS_REQUEST]: parkingLocationsRequest,
  [Types.PARKING_LOCATIONS_SUCCESS]: parkingLocationsSuccess,
  [Types.PARKING_LOCATIONS_FAILURE]: parkingLocationsFailure,

  [Types.SEARCH_LOCATIONS_REQUEST]: searchLocationsRequest,
  [Types.SEARCH_LOCATIONS_SUCCESS]: searchLocationsSuccess,
  [Types.SEARCH_LOCATIONS_FAILURE]: searchLocationsFailure,

  [Types.GET_PARKING_DETAIL_REQUEST]: getParkingDetailRequest,
  [Types.GET_PARKING_DETAIL_SUCCESS]: getParkingDetailSuccess,
  [Types.GET_PARKING_DETAIL_FAILURE]: getParkingDetailFailure,

  [Types.CHECK_IN_REQUEST]: checkInRequest,
  [Types.CHECK_IN_SUCCESS]: checkInSuccess,
  [Types.CHECK_IN_FAILURE]: checkInFailure,

  [Types.GET_CARS_REQUEST]: getCarsRequest,
  [Types.GET_CARS_SUCCESS]: getCarsSuccess,
  [Types.GET_CARS_FAILURE]: getCarsFailure
})
