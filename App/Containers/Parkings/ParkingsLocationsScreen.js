import React, { Component } from 'react'
import { TouchableOpacity } from 'react-native'
import {connect} from 'react-redux'
// libraries
import MapView, { Marker, Callout } from 'react-native-maps'
import Icon from 'react-native-vector-icons/Ionicons'

// components
import ParkingCard from '../components/ParkingCard'
import CustomMarker from '../components/Marker'
// actions
import ParkingLocationsActions from './ParkingLocationsRedux'
// fixtures
import Map from '../../Fixtures/map'
import MapLayout from '../../Components/MapLayout'
// styles
import styles from '../../assets/Styles/index'
import I18n from '../../I18n'
import { Colors } from '../../assets/Themes'
class ParkingsLocationsScreen extends Component {
  state = {
    parkingLocations: []
  };
  componentDidMount () {
    this.handleParkingLocations()
  }
  handleParkingLocations = () => {
    this.props.getParkingLocations()
  };
  render () {
    const { loggedIn, parkingLocations } = this.props
    return (
      <MapLayout>
        <MapView
          style={styles.map}
          customMapStyle={Map.style}
          showsUserLocation={false}
          followsUserLocation={false}
          region={{
            latitude: Map.latitude,
            longitude: Map.longitude,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121
          }}>
          {
            parkingLocations && parkingLocations.map(marker => {
              const coordinate = {
                latitude: marker.latitude,
                longitude: marker.longitude
              }
              return (
                <Marker
                  key={marker.id}
                  coordinate={coordinate}
                                >
                  <CustomMarker marker={marker} />
                  <Callout tooltip onPress={() => {
                    if (loggedIn) {
                      this.props.navigation.navigate('ParkingLotDetailScreen', { parkingId: marker.id, name: marker.name})
                    } else {
                      this.props.navigation.navigate('LoginScreen', { parkingId: marker.id, name: marker.name})
                    }
                  }}>
                    <ParkingCard marker={marker} />
                  </Callout>
                </Marker>
              )
            })
                    }
        </MapView>
      </MapLayout>
    )
  }
}

ParkingsLocationsScreen.navigationOptions = (props) => {
  return ({
    title: I18n.t('Smart Parking'),
    headerRight: (
      <TouchableOpacity onPress={() => { props.navigation.navigate('ParkingSearchScreen') }} style={styles.buttonHeader}>
        <Icon name={'ios-search-outline'} style={{marginRight: 15}} size={30} color={Colors.snow} />
      </TouchableOpacity>
    )
  })
}

const mapStateToProps = (state) => ({
  parkingLocations: state.parking.parkingLocations,
  fetching: state.parking.fetching,
  error: state.parking.error,
  loggedIn: state.auth.loggedIn
})
const mapDispatchToProps = (dispatch) => ({
  getParkingLocations: () => dispatch(ParkingLocationsActions.parkingLocationsRequest())
})
export default connect(mapStateToProps, mapDispatchToProps)(ParkingsLocationsScreen)
