import { call, put, all } from 'redux-saga/effects'
import ParkingLocationsActions from './ParkingLocationsRedux'
import ParkingCarsActions from '../ParkingCars/ParkingCarsRedux'
import { NavigationActions } from 'react-navigation'

export function * getParkingLocations (api, action) {
  // make the call to the api
  const response = yield call(api.getParkingLocations)
  if (response.ok) {
    const {data} = response
    // do data conversion here if needed
    yield put(ParkingLocationsActions.parkingLocationsSuccess(data))
  } else {
    yield put(ParkingLocationsActions.parkingLocationsFailure())
  }
}

export function * searchParkingLocations (api, action) {
  // make the call to the api
  const {query} = action
  const response = yield call(api.searchParkingLocations, query)
  if (response.ok) {
    const {data} = response
    // do data conversion here if needed
    yield put(ParkingLocationsActions.searchLocationsSuccess(data))
  } else {
    yield put(ParkingLocationsActions.searchLocationsFailure())
  }
}
export function * getParkingDetail (api, action) {
  // make the call to the api
  const {parkingId} = action
  const response = yield call(api.getParkingDetail, parkingId)
  if (response.ok) {
    const {data} = response
    // do data conversion here if needed
    yield put(ParkingLocationsActions.getParkingDetailSuccess(data))
  } else {
    yield put(ParkingLocationsActions.getParkingDetailFailure())
  }
}

export function * checkIn (api, action) {
  // make the call to the api
  const {data} = action
  const response = yield call(api.checkIn, data)
  if (response.ok) {
    console.log(response)
    // do data conversion here if needed
    yield put(ParkingLocationsActions.checkInSuccess())
    yield put(ParkingCarsActions.parkingCarsRequest())
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: 'Home',
          action: NavigationActions.navigate({
            routeName: 'ParkingCars'
          })
        })
      ]
    })
    yield put(resetAction)
  } else {
    yield put(ParkingLocationsActions.checkInFailure())
  }
}
