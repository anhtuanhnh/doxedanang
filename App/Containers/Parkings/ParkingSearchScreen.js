import React, { Component } from 'react'
import { View, FlatList, TouchableOpacity, Text, ActivityIndicator } from 'react-native'
import CleanLayout from '../../Components/CleanLayout'
import styles from '../../assets/Styles'
import Input from '../../Components/Input'
import { Colors, Metrics } from '../../assets/Themes/index'
import ParkingCard from '../components/ParkingCard'
import Icon from 'react-native-vector-icons/Ionicons'

import {connect} from 'react-redux'
import { createAnimatableComponent } from 'react-native-animatable'
import ParkingLocationsActions from './ParkingLocationsRedux'
import I18n from '../../I18n'
const AnimatableSectionList = createAnimatableComponent(FlatList)

class ParkingSearchScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      search: ''
    }
  }
  searchParking = (query = '') => {
    setTimeout(() => {
      this.props.searchParkingLocations(query)
    }, 500)
  }
  searchResult = (item) => (
    <TouchableOpacity onPress={() => {
      if (this.props.loggedIn) {
        this.props.navigation.navigate('ParkingLotDetailScreen', { parkingId: item.id, name: item.name})
      } else {
        this.props.navigation.navigate('LoginScreen', { parkingId: item.id, name: item.name})
      }
    }}>
      <ParkingCard marker={item} isHeading={false} styleCard={styles.card} styleItem={styles.subCard} />
    </TouchableOpacity>
    )
  renderSeparator = () => {
    return <View style={{height: 15}} />
  }
  renderHeader = (length) => (
    <Text style={styles.pldFormLabel}>
      {I18n.t(`There are ${length} accurate result`.toUpperCase())}
    </Text>
    )
  render () {
    const { search } = this.state
    const { searchLocations, parkingLocations, fetching } = this.props
    return (
      <CleanLayout
        fullLoading={fetching}
      >
        <Input baseInput value={search} styleInput={{paddingLeft: 50}} placeHolder={I18n.t('Enter key search')} iconInput={'search-xanh'} iconLeft={Colors.default} press={(value) => { this.setState({search: value}); this.searchParking(value) }} />
        <FlatList
          animation='bounceIn'
          duration={2100}
          delay={500}
          data={searchLocations || parkingLocations}
          style={{paddingHorizontal: 20, marginBottom: 80}}
          keyExtractor={item => item.id.toString()}
          showsVerticalScrollIndicator={false}
          renderItem={({item}) => this.searchResult(item)}
          ItemSeparatorComponent={this.renderSeparator}
          ListHeaderComponent={() => this.renderHeader(searchLocations ? searchLocations.length : parkingLocations.length)}
          // ListFooterComponent={this.renderFooter}
        />
      </CleanLayout>
    )
  }
}

ParkingSearchScreen.navigationOptions = (props) => {
  return ({
    title: I18n.t('Tìm kiếm'),
    headerRight: (
      <TouchableOpacity onPress={() => { props.getParkingLocations() }} style={styles.buttonHeader}>
        <Icon name={'ios-refresh-circle'} style={{marginRight: 15}} size={30} color={Colors.snow} />
      </TouchableOpacity>
    )
  })
}
const mapStateToProps = (state) => ({
  loggedIn: state.auth.loggedIn,
  parkingLocations: state.parking.parkingLocations,
  searchLocations: state.parking.searchLocations,
  fetching: state.parking.fetchingSearch
})
const mapDispatchToProps = (dispatch) => ({
  searchParkingLocations: (query) => dispatch(ParkingLocationsActions.searchLocationsRequest(query)),
  getParkingLocations: () => dispatch(ParkingLocationsActions.parkingLocationsRequest())
})
export default connect(mapStateToProps, mapDispatchToProps)(ParkingSearchScreen)
