import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
// components
import BaseLayout from '../../Components/BaseLayout'
import styles from '../../assets/Styles/index'
import Input from '../../Components/Input'
import NormalButton from '../../Components/NormalButton'
// actions
import toast from '../../Components/Toast'
import { connect } from 'react-redux'
import { Colors } from '../../assets/Themes/index'
import axios from 'axios/index'
import I18n from '../../I18n/index'
const API_URL = 'https://dev-api.doxedanang.com/api'

class ForgetPasswordScreen extends Component {
  constructor () {
    super()
    this.state = {
      phone: ''
    }
  }
  resetPassword = async () => {
    const { getOldPhone } = this.props.navigation.state.params
    let { phone } = this.state
    if (phone.slice(0, 1) === '0') {
      phone = await `+84${phone.slice(1, phone.length)}`
    } else {
      phone = await `+84${phone}`
    }
    console.log(phone)

    axios.post(`${API_URL}/account/reset_password/init`, phone, {
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
        console.log(res)
        this.props.navigation.navigate('ForgetPasswordFinishScreen', { myPhone: phone, getOldPhone })
      })
      .catch(error => {
        toast(error)
      })
  }
  render () {
    const { phone } = this.state
    return (
      <BaseLayout>
        <Text style={styles.textCenter}>{I18n.t('Please enter your phone number in the box below so that we can help you')}</Text>
        <Input
          baseInput
          styleInput={styles.baseInput}
          value={phone}
          iconInput={'dien-thoai'}
          press={(value) => this.setState({phone: value})} />
        <NormalButton onPress={this.resetPassword} text={I18n.t('Sent request')} />
        <View style={[styles.rowCenter, {paddingTop: 20}]}>
          <Text>{I18n.t('Dont Have an Account?')}</Text>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('RegisterScreen')}><Text style={styles.textSpecial}>{I18n.t('Register now')}</Text></TouchableOpacity>
        </View>
      </BaseLayout>
    )
  }
}
const mapStateToProps = (state) => ({
  fetching: state.auth.fetching
})

ForgetPasswordScreen.navigationOptions = (props) => {
  return ({
    title: I18n.t('Forget passwod')
  })
}
export default connect(mapStateToProps, null)(ForgetPasswordScreen)
