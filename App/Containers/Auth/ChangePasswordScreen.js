import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import {connect} from 'react-redux'
// libraries
import Config from 'react-native-config/index'
import Validator from 'validatorjs'
// components
import BaseLayout from '../../Components/BaseLayout'
import styles from '../../assets/Styles/index'
import Input from '../../Components/Input'
import FullButton from '../../Components/FullButton'
import toast from '../../Components/Toast'
// styles
import {Metrics} from '../../assets/Themes'
// actions
import I18n from '../../I18n'
import AuthActions from './AuthRedux'

class ChangePasswordScreen extends Component {
  constructor () {
    super()
    this.state = {
      password: '',
      confirmPassword: '',
      errors: {}
    }
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.success) {
      toast(I18n.t('Change password success'))
      this.props.navigation.goBack(null)
    }
    if (nextProps.error) {
      toast(I18n.t('Change password success'))
    }
  }
  changePassword = () => {
    const { password, confirmPassword } = this.state
    let dataPass = {
      password,
      confirmPassword
    }
    let rules = {
      password: 'required|min:4',
      confirmPassword: 'required|min:4|same:password'
    }
    let validation = new Validator(dataPass, rules)
    if (validation.passes()) {
      this.setState({errors: {}})
      this.props.changePassword(password)
    } else {
      this.setState({errors: validation.errors.errors})
    }
  }
  render () {
    const { confirmPassword, password, errors } = this.state
    return (
      <BaseLayout>
        <Text style={styles.textCenter}>{I18n.t('To change your password, please fill in the information below')}</Text>
        <Input baseInput styleInput={styles.baseInput} value={password} errors={errors.password} iconInput={'mat-khau'} secureTextEntry iconInputRight={'show-mk'} press={(value) => this.setState({password: value})} />
        <Input baseInput styleInput={styles.baseInput} value={confirmPassword} errors={errors.confirmPassword} iconInput={'mat-khau'} secureTextEntry iconInputRight={'show-mk'} press={(value) => this.setState({confirmPassword: value})} />
        <FullButton onPress={this.changePassword} text={I18n.t('Change Password').toUpperCase()} />

      </BaseLayout>
    )
  }
}
const mapStateToProps = (state) => ({
  fetching: state.auth.fetching,
  success: state.auth.success,
  error: state.auth.error
})
const mapDispatchToProps = (dispatch) => ({
  changePassword: (password) => dispatch(AuthActions.changePasswordRequest(password))
})

ChangePasswordScreen.navigationOptions = {
  title: I18n.t('Change Password')
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangePasswordScreen)
