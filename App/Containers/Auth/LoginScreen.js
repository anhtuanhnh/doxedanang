import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Alert, AsyncStorage } from 'react-native'
// libraries
import {connect} from 'react-redux'
import Modal from 'react-native-modalbox'
import Icon from 'react-native-vector-icons/Ionicons'
import Icons from '../../Components/AppIcons'
import I18n from '../../I18n/index'

import Validator from 'validatorjs'
// actions
import AuthActions from './AuthRedux'
// components
import Input from '../../Components/Input'
import NormalButton from '../../Components/NormalButton'
import BaseLayout from '../../Components/BaseLayout'
import toast from '../../Components/Toast'
// styles
// import Icons from '../../Components/AppIcons'
import { Fonts, Colors, Metrics, ApplicationStyles } from '../../assets/Themes/index'
import styles from './LoginScreenStyles'
import { NavigationActions } from 'react-navigation'

class LoginScreen extends Component {
  constructor (props) {
    super(props)
    const { params } = props.navigation.state
    const route = params ? params.route : 'AppStack'
    this.state = {
      route,
      phone: '+84976971372',
      password: '1234',
      autoFocus: false,
      isOpen: false,
      swipeToClose: true,
      sliderValue: 0.3,
      disable: true,
      errors: {}
    }
  }
  componentWillMount () {
    this.getOldPhone()
  }
  componentWillReceiveProps (nextProps) {
    const { params } = this.props.navigation.state
    if (nextProps.loggedIn) {
      toast(I18n.t('Login success'))
      if (params && params.parkingId) {
        const resetAction = NavigationActions.reset({
          index: 1,
          actions: [
            NavigationActions.navigate({ routeName: 'Home' }),
            NavigationActions.navigate({
              routeName: 'ParkingLotDetailScreen',
              params: {
                parkingId: params.parkingId,
                name: params.name
              }
            })
          ]
        })
        this.props.navigation.dispatch(resetAction)
      } else {
        this.props.navigation.goBack(null)
      }
    }
  }
  componentDidMount () {
    this.initValidator()
  }
  getOldPhone = async () => {
    const value = await AsyncStorage.getItem('myPhone')
    if (value !== null) {
      this.setState({phone: value})
    }
  }
  signIn = () => {
    const data = {
      phone: this.state.phone,
      password: this.state.password
    }
    if (data.phone.slice(0, 1) === '0') {
      data.phone = `+84${data.phone.slice(1, data.phone.length)}`
      this.props.login(data)
    } else if (data.phone.slice(0, 3) === '+84') {
      this.props.login(data)
    } else {
      data.phone = `+84${data.phone}`
      this.props.login(data)
    }
  };
  initValidator = () => {
    let data = {
      phone: this.state.phone,
      password: this.state.password
    }
    let rules = {
      password: 'required|min:4',
      phone: 'required|min:8'
    }
    let validation = new Validator(data, rules)
    if (validation.passes()) {
      this.setState({disable: false})
    } else {
      this.setState({disable: true})
    }
  }
  validatorInput = () => {
    let data = {
      phone: this.state.phone,
      password: this.state.password
    }
    let rules = {
      password: 'required|min:4',
      phone: 'required|min:8'
    }
    let validation = new Validator(data, rules)
    if (validation.passes()) {
      this.setState({errors: {}, disable: false})
    } else {
      this.setState({errors: validation.errors.errors, disable: true})
    }
  }
  checkExistPhone = () => {
    const { phone } = this.state
    this.nodeRef.close()
    if (phone.length > 9) {
      this.props.navigation.navigate('ActiveAccountScreen', {myPhone: phone})
    } else {
      Alert.alert(
                I18n.t('SmartParking alerts'),
                I18n.t('You must enter phone number before verify account'),
        [
          {text: I18n.t('OK'),
            onPress: () => {
              this.setState({autoFocus: true})
            }}
        ],
                { cancelable: false }
            )
    }
  }
  render () {
    const { phone, password, isOpen, autoFocus, route, errors, disable } = this.state
    const { fetching } = this.props
    return (
      <BaseLayout
        fullLoading={fetching}
      >
        <Modal
          style={[styles.modal, {zIndex: 100}]}
          ref={n => { this.nodeRef = n }}
          isOpen={isOpen}
          swipeToClose
          position={'center'}
          coverScreen>
          <View style={styles.modalTitle}>
            <Text style={{fontWeight: '600'}}>{I18n.t('Have you trouble when log in?')}</Text>
          </View>
          <TouchableOpacity style={styles.modalAction} onPress={() => { this.nodeRef.close(); this.props.navigation.navigate('ForgetPasswordScreen', {getOldPhone: this.getOldPhone}) }}>
            <Text style={{flex: 1}}>{I18n.t('Forgot password')}</Text>
            <Icon name={'md-arrow-forward'} size={18} color={Colors.default} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.modalAction} onPress={this.checkExistPhone}>
            <Text style={{flex: 1}}>{I18n.t('Not confirm account yet?')}</Text>
            <Icon name={'md-arrow-forward'} size={18} color={Colors.default} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.modalFooter} onPress={() => { this.nodeRef.close() }}>
            <Text style={styles.moreText}>{I18n.t('Close')}</Text>
          </TouchableOpacity>
        </Modal>
        <Input
          baseInput
          styleInput={styles.pldFormInput}
          value={phone}
          autoFocus={autoFocus}
          errors={errors.phone}
          iconInput={'dien-thoai'}
          maxLength={13}
          returnKeyType='next'
          autoCapitalize='none'
          autoCorrect={false}
          onSubmitEditing={() => this.nodePassword.focus()}
          onEndEditing={this.validatorInput}
          press={(value) => this.setState({phone: value})}
          keyboardType={'phone-pad'}
        />
        <Input
          baseInput
          styleInput={styles.pldFormInput}
          value={password}
          iconInput={'mat-khau'}
          errors={errors.password}
          secureTextEntry
          clearTextOnFocus
          refNode={n => { this.nodePassword = n }}
          keyboardType='default'
          returnKeyType='go'
          autoCapitalize='none'
          autoCorrect={false}
          onEndEditing={this.validatorInput}
          iconInputRight={'show-mk'}
          press={(value) => this.setState({password: value})}
        />
        <NormalButton disable={this.props.fetching} onPress={this.signIn} text={I18n.t('Sign In')} />
        <TouchableOpacity style={styles.boxMore} onPress={() => { this.props.navigation.navigate('RegisterScreen') }}>
          <View style={styles.moreLeft}>
            <Text style={styles.moreText}>{I18n.t('Dont Have an Account?')}</Text>
            <View style={styles.moreAction} >
              <Text style={[styles.moreText, styles.textSpecial]}>
                {I18n.t('Register now')}
              </Text>
            </View>
          </View>
          <TouchableOpacity onPress={() => { this.nodeRef.open() }} style={styles.moreRight}>
            <Text style={styles.moreText}>{I18n.t('Login fail')}</Text>
            <View>
              <Text style={[styles.moreText, styles.textSpecial]}>
                {I18n.t('Try now')}
              </Text>
            </View>
          </TouchableOpacity>
        </TouchableOpacity>
      </BaseLayout>
    )
  }
}
const mapStateToProps = (state) => ({
  loggedIn: state.auth.loggedIn,
  error: state.auth.error,
  fetching: state.auth.fetchingLogin
})
const mapDispatchToProps = (dispatch) => ({
  login: (user) => dispatch(AuthActions.loginRequest(user))
})

LoginScreen.navigationOptions = (props) => {
  return ({
    title: I18n.t('Login')
  })
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
