import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
// libraries
import axios from 'axios/index'
import Config from 'react-native-config/index'
// components
import toast from '../../Components/Toast'
import BaseLayout from '../../Components/BaseLayout'
import styles from '../../assets/Styles/index'
import Input from '../../Components/Input'
import NormalButton from '../../Components/NormalButton'
import { connect } from 'react-redux'
import I18n from '../../I18n'
const API_URL = 'https://dev-api.doxedanang.com/api'

class ForgetPasswordFinishScreen extends Component {
  constructor (props) {
    super(props)
    const { myPhone } = props.navigation.state.params
    const { getOldPhone } = props.navigation.state.params
    this.state = {
      code: '',
      myPhone,
      newPassword: ''
    }
    this.getOldPhone = getOldPhone
  }
  componentWillMount () {
    const {myPhone} = this.props.navigation.state.params
    if (!myPhone) {
      this.props.navigation.goBack(null)
    }
  }
  resendCode = () => {
    const { myPhone } = this.state
    axios.post(`${API_URL}/account/reset_password/init`, myPhone, {
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
        toast(I18n.t('Confirm code has been resent'))
      })
      .catch(error => {
        if (error.response)toast(error.response.data.errorMessage)
      })
  };
  confirm = () => {
    const { code, newPassword } = this.state
    if (code.length > 0) {
      axios.post(`${API_URL}/account/reset_password/finish`, {
        key: code,
        newPassword
      })
        .then(res => {
          toast(I18n.t('Create new password success'))
          this.getOldPhone()
          this.props.navigation.goBack(2)
        })
        .catch(error => {
          toast(error)
        })
    } else {
      toast(I18n.t('Please enter the confirm code'))
    }
  };
  render () {
    const { code, myPhone, newPassword } = this.state
    return (
      <BaseLayout>
        <Text style={styles.textCenter}>
          {I18n.t(`An active code was sent`)}
          <Text style={styles.textBold}>{myPhone}</Text>
          {I18n.t(`(Vietnam),Please enter the confirm code and create new password.The code will be discarded within 10 minutes since since sent.`)}</Text>
        <Input
          baseInput
          autoFocus
          styleInput={styles.baseInput}
          value={code}
          keyboardType={'number-pad'}
          iconInput={'ma-kich-hoat'}
          returnKeyType='next'
          autoCapitalize='none'
          autoCorrect={false}
          onSubmitEditing={() => this.nodePassword.focus()}
          press={(value) => this.setState({code: value})} />
        <Input
          baseInput
          styleInput={styles.pldFormInput}
          value={newPassword}
          iconInput={'mat-khau'}
          secureTextEntry
          refNode={n => { this.nodePassword = n }}
          keyboardType='default'
          returnKeyType='go'
          autoCapitalize='none'
          autoCorrect={false}
          iconInputRight={'show-mk'}
          press={(value) => this.setState({newPassword: value})} />
        <NormalButton onPress={this.confirm} text={I18n.t('Confirm')} />
        <View style={[styles.rowCenter]}>
          <Text>{I18n.t('Not recieve active code ,do you ?')}</Text>
          <TouchableOpacity onPress={this.resendCode}><Text style={styles.textSpecial}>{I18n('Resent code')}</Text></TouchableOpacity>
        </View>
      </BaseLayout>
    )
  }
}
ForgetPasswordFinishScreen.navigationOptions = (props) => {
  return ({
    title: I18n.t('Forgot password')
  })
}
const mapStateToProps = (state) => ({
  fetching: state.auth.fetching
})

export default connect(mapStateToProps, null)(ForgetPasswordFinishScreen)
