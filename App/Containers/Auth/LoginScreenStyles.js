import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles } from '../../Themes/index'
import { Colors } from '../../assets/Themes/index'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  buttonHeader: {
    height: 64,
    width: 64,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'red'
  },
  boxMore: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    flex: 1,
    marginTop: 0.081 * Metrics.screenHeight
  },
  moreLeft: {
    flex: 1
  },
  moreRight: {
    flex: 1,
    alignItems: 'flex-end'
  },
  moreText: {
    fontSize: 14,
    lineHeight: 20
  },
  moreAction: {
  },
  modal: {
    width: 0.78 * Metrics.screenWidth,
    height: 0.34 * Metrics.screenHeight,
    zIndex: 100
  },
  modalTitle: {
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: 'center'
  },
  modalAction: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    flex: 1,
    borderTopWidth: 1,
    borderTopColor: '#E7EAF0',
    paddingHorizontal: 20,
    alignItems: 'center'
  },
  modalFooter: {
    borderTopWidth: 1,
    borderTopColor: '#E7EAF0',
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: 'center'
  },
  textSpecial: {
    color: Colors.default,
    // fontFamily: Roboto-Medium;
    fontSize: 14,
    lineHeight: 20,
    textDecorationStyle: 'solid',
    textDecorationLine: 'underline'
  }

})
