import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  loginRequest: ['user'],
  loginSuccess: ['token'],
  loginFailure: null,
  registerRequest: ['user'],
  registerSuccess: null,
  registerFailure: ['error'],

  logOut: null,
  // resetPasswordInitRequest: ['user'],
  // resetPasswordInitSuccess: null,
  // resetPasswordInitFailure: null,
  // resetPasswordFinishRequest: ['user'],
  // resetPasswordFinishSuccess: null,
  // resetPasswordFinishFailure: null,
  changePasswordRequest: ['password'],
  changePasswordSuccess: null,
  changePasswordFailure: null
  // activateRequest: ['user'],
  // activateSuccess: null,
  // activateFailure: null,
  // resendCodeRequest: ['user'],
  // resendCodeSuccess: null,
  // resendCodeFailure: null,
})

export const AuthTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  token: null,
  fetchingLogin: null,
  error: null,
  profile: null,
  loggedIn: null,
  success: null
})

/* ------------- Selectors ------------- */

// export const GithubSelectors = {
//   selectAvatar: state => state.github.avatar
// }

/* ------------- Reducers ------------- */

// login
export const loginRequest = (state) =>
  state.merge({fetchingLogin: true, token: null, loggedIn: null, error: null})

export const loginSuccess = (state, action) => {
  const { token } = action
  return state.merge({ fetchingLogin: false, error: null, loggedIn: true, token })
}

export const loginFailure = (state, action) => {
  state.merge({ fetchingLogin: false, error: true, loggedIn: false, token: null })
}

// register
export const registerRequest = (state) =>
  state.merge({ fetching: true })

export const registerSuccess = (state, action) => {
  return state.merge({fetching: false, error: null, success: true})
}

export const registerFailure = (state, action) => {
  const { error } = action
  console.log(error)
  let errorMessage = ''
  if (error.response) {
    errorMessage = error.response.data.errorMessage
  } else {
    errorMessage = 'Đăng ký thất bại'
  }
  state.merge({ fetching: false, error: errorMessage, success: null })
}

// change password
export const changePasswordRequest = (state) =>
  state.merge({ fetching: true, success: null })

export const changePasswordSuccess = (state, action) => {
  return state.merge({ fetching: false, error: null, success: true })
}

export const changePasswordFailure = (state) =>
  state.merge({ fetching: false, error: true })

export const logOut = (state) =>
  state.merge({ loggedIn: false, token: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOG_OUT]: logOut,
  [Types.LOGIN_REQUEST]: loginRequest,
  [Types.LOGIN_SUCCESS]: loginSuccess,
  [Types.LOGIN_FAILURE]: loginFailure,
  [Types.REGISTER_REQUEST]: registerRequest,
  [Types.REGISTER_SUCCESS]: registerSuccess,
  [Types.REGISTER_FAILURE]: registerFailure,
  [Types.CHANGE_PASSWORD_REQUEST]: changePasswordRequest,
  [Types.CHANGE_PASSWORD_SUCCESS]: changePasswordSuccess,
  [Types.CHANGE_PASSWORD_FAILURE]: changePasswordFailure
})
