import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
// libraries
import axios from 'axios/index'
// actions
// components
import toast from '../../Components/Toast'
import BaseLayout from '../../Components/BaseLayout'
import styles from '../../assets/Styles/index'
import Input from '../../Components/Input'
import NormalButton from '../../Components/NormalButton'
import I18n from '../../I18n'
const API_URL = 'https://dev-api.doxedanang.com/api'

class ActiveAccountScreen extends Component {
  constructor (props) {
    super(props)
    const {myPhone} = props.navigation.state.params
    this.state = {
      code: '',
      myPhone
    }
  }
  resendCode = () => {
    const { myPhone } = this.state
    axios.post(`${API_URL}/resend-activation-key`, myPhone)
            .then(res => {
              toast(I18n.t('Confirm code has been resent'))
            })
            .catch(error => {
              if (error.response)toast(error.response.data.errorMessage)
            })
  }
  confirm = () => {
    const { code } = this.state
    if (code.length > 0) {
      axios.get(`${API_URL}/activate?key=${code}`)
        .then(res => {
          this.props.navigation.pop(1)
          toast(I18n.t('Your account have been active, please re login'))
        })
        .catch(error => {
          if (error.response) toast(error.response.data.errorMessage | error.response.data.description)
        })
    } else {
      toast(I18n.t('Please enter the confirm code'))
    }
  }
  render () {
    const { code, myPhone } = this.state
    return (
      <BaseLayout>
        <Text style={styles.textCenter}>{I18n.t(`An active code was sent `)} <Text style={styles.textBold}>{myPhone}</Text> {` (Việt Nam), vui lòng nhập mã và hoàn tất đăng ký.`}</Text>
        <Input
          baseInput
          autoFocus
          styleInput={styles.baseInput}
          value={code}
          iconInput={'ma-kich-hoat'}
          press={(value) => this.setState({code: value})}
          keyboardType={'number-pad'}
                />
        <NormalButton onPress={this.confirm} text={I18n.t('Confirm')} />
        <View style={[styles.rowCenter]}>
          <Text>{I18n.t('Not recieve active code ,do you ?')}</Text>
          <TouchableOpacity onPress={this.resendCode}><Text style={styles.textSpecial}>{I18n.t('Resent code')}</Text></TouchableOpacity>
        </View>
      </BaseLayout>
    )
  }
}
ActiveAccountScreen.navigationOptions = {
  title: I18n.t('Active')
}
export default ActiveAccountScreen
