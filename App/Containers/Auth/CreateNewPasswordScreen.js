import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
// components
import BaseLayout from '../../Components/BaseLayout'
import styles from '../../assets/Styles/index'
import Input from '../../Components/Input'
import NormalButton from '../../Components/NormalButton'
// actions
import * as actions from '../../Actions'
import I18n from '../../I18n'
class CreateNewPasswordScreen extends Component {
  constructor () {
    super()
    this.state = {
      phone: '0905031632'
    }
  }
  signUp = () => {
    const { phone } = this.state
    actions.auth.resetPasswordInit(phone)
  }
  render () {
    const { phone, password } = this.state
    return (
      <BaseLayout
        header={{
          title: I18n.t('Create new password'),
          headerType: 'normal',
          buttonLeftOnPress: () => { this.props.navigation.goBack(null) },
          buttonLeftType: 'back'
        }}
            >
        <Text style={styles.textCenter}>{I18n.t('Please enter your phone number in the box below so that we can help you')}</Text>
        <Input baseInput styleInput={styles.baseInput} value={phone} iconInput={'dien-thoai'} press={(value) => this.setState({phone: value})} />
        <NormalButton onPress={this.signUp} text={I18n.t('Sent request')} />
        <View style={[styles.rowCenter, { paddingTop: 20}]}>
          <Text>{I18n.t('Dont Have an Account?')}</Text>
          <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}><Text style={styles.textSpecial}>{' Đăng ký ngay'}</Text></TouchableOpacity>
        </View>
      </BaseLayout>
    )
  }
}

export default CreateNewPasswordScreen
