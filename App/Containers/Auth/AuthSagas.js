import { call, put, all } from 'redux-saga/effects'
import AuthActions from './AuthRedux'
import AccountActions from '../Account/AccountRedux'
import NotificationActions from '../Notifications/NotificationsRedux'
import ParkingCarsActions from '../ParkingCars/ParkingCarsRedux'
import ParkingLocationsActions from '../Parkings/ParkingLocationsRedux'
import { NavigationActions } from 'react-navigation'
import toast from '../../Components/Toast'

export function * loginUser (api, action) {
  try {
    const { user } = action
    // make the call to the api
    const response = yield call(api.loginUser, user)
    const { data } = response
    if (response.ok) {
      const token = data.id_token
      yield call(api.setAuthorization, token)
      yield put(AuthActions.loginSuccess(token))
      yield all([ put(AccountActions.getAccountRequest()),
        put(AccountActions.getWalletRequest()),
        put(ParkingCarsActions.parkingCarsRequest()),
        put(ParkingLocationsActions.getCarsRequest()),
        put(NotificationActions.getNotificationRequest())
      ])
      // do data conversion here if needed
    } else {
      let errorMessage = ''
      if (data.error.response) {
        errorMessage = data.error.response.data.errorMessage
      } else {
        errorMessage = 'Đăng ký thất bại'
      }
      toast(errorMessage)
      yield put(AuthActions.loginFailure())
    }
  } catch (e) {
    console.tron.display({
      name: 'loginUser error',
      preview: 'expand',
      value: {
        message: e.message,
        e: e
      }
    })
  }
}

export function * registerUser (api, action) {
  try {
    const { user } = action
    // make the call to the api
    const response = yield call(api.registerUser, user)
    const { data } = response

    if (response.ok) {
      // do data conversion here if needed
      toast('Đăng ký thành công')
      yield put(AuthActions.registerSuccess())
    } else {
      let errorMessage = ''
      if (data.error.response) {
        errorMessage = data.error.response.data.errorMessage
      } else {
        errorMessage = 'Đăng ký thất bại'
      }
      toast(errorMessage)
      yield put(AuthActions.registerFailure())
    }
  } catch (e) {
    console.tron.display({
      name: 'registerUser error',
      preview: 'expand',
      value: {
        message: e.message,
        e: e
      }
    })
  }
}
export function * changePasswordUser (api, action) {
  try {
    const { password } = action
    // make the call to the api
    const response = yield call(api.changePasswordUser, password)
    const { data } = response

    if (response.ok) {
      // do data conversion here if needed
      toast('Đổi mật khẩu thành công')
      yield put(AuthActions.changePasswordSuccess())
    } else {
      let errorMessage = ''
      if (data.error.response) {
        errorMessage = data.error.response.data.errorMessage
      } else {
        errorMessage = 'Đổi mật khẩu thất bại'
      }
      toast(errorMessage)

      yield put(AuthActions.changePasswordFailure())
    }
  } catch (e) {
    console.tron.display({
      name: 'changePasswordUser error',
      preview: 'expand',
      value: {
        message: e.message,
        e: e
      }
    })
  }
}
