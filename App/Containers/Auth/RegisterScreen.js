import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, Text, TouchableOpacity } from 'react-native'
// libraries
import Validator from 'validatorjs'
import I18n from '../../I18n/index'
// components
import BaseLayout from '../../Components/BaseLayout'
import Input from '../../Components/Input'
import NormalButton from '../../Components/NormalButton'
import toast from '../../Components/Toast'
// actions
import AuthActions from './AuthRedux'
// styles
import styles from '../../assets/Styles/index'

class RegisterScreen extends Component {
  constructor () {
    super()
    this.state = {
      phone: '',
      password: '',
      disable: true,
      errors: {}
    }
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.error) {
      toast(nextProps.error)
    }
    if (nextProps.success) {
      toast(I18n.t('Register success'))
      this.props.navigation.goBack(null)
    }
  }
  componentDidMount () {
    this.initValidatorInput()
  }
  initValidatorInput = () => {
    let data = {
      phone: this.state.phone,
      password: this.state.password
    }
    let rules = {
      password: 'required|min:4',
      phone: 'required|min:8'
    }
    let validation = new Validator(data, rules)
    if (validation.passes()) {
      this.setState({disable: false})
    } else {
      this.setState({disable: true})
    }
  }
  validatorInput = () => {
    let data = {
      phone: this.state.phone,
      password: this.state.password
    }
    let rules = {
      password: 'required|min:4',
      phone: 'required|min:8'
    }
    let validation = new Validator(data, rules)
    if (validation.passes()) {
      this.setState({errors: {}, disable: false})
    } else {
      this.setState({errors: validation.errors.errors, disable: true})
    }
  }
  requestSignUp = () => {
    this.setState({disable: true})
    const { phone, password } = this.state
    const data = {
      langKey: 'en',
      phone,
      password
    }
    if (phone.slice(0, 1) === '0') {
      data.phone = `+84${data.phone.slice(1, data.phone.length)}`
      this.signUp(data)
    } else {
      data.phone = `+84${data.phone}`
      this.signUp(data)
    }
  }
  signUp = (data) => {
    this.props.register(data)
  }
  render () {
    const { phone, password, errors, disable } = this.state
    const { fetching } = this.props
    return (
      <BaseLayout
        fullLoading={fetching}
      >
        <Text style={styles.textCenter}>{I18n.t('Please fill in accurate infomation below')}</Text>
        <Input
          baseInput
          styleInput={styles.baseInput}
          value={phone}
          errors={errors.phone}
          iconInput={'dien-thoai'}
          press={(value) => this.setState({phone: value})}
          keyboardType={'phone-pad'}
          maxLength={11}
          onEndEditing={this.validatorInput} />
        <Input
          baseInput
          styleInput={styles.baseInput}
          value={password}
          errors={errors.password}
          iconInput={'mat-khau'}
          secureTextEntry
          onEndEditing={this.validatorInput}
          iconInputRight={'show-mk'}
          press={(value) => this.setState({password: value})} />
        <NormalButton disable={disable} onPress={this.requestSignUp} text={I18n.t('Register')} />
        <View style={[styles.rowCenter]}>
          <Text>{I18n.t('Have an account?')}</Text>
          <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}><Text style={styles.textSpecial}>{'Đăng nhập ngay'}</Text></TouchableOpacity>
        </View>
      </BaseLayout>
    )
  }
}

const mapStateToProps = (state) => ({
  loggedIn: state.auth.loggedIn,
  fetching: state.auth.fetching
})
const mapDispatchToProps = (dispatch) => ({
  register: (user) => dispatch(AuthActions.registerRequest(user))
})

RegisterScreen.navigationOptions = {
  title: I18n.t('Register')

}
export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen)
