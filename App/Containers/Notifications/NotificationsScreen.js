import React, { Component } from 'react'
import { View, FlatList, Text, Image, RefreshControl } from 'react-native'
import {connect} from 'react-redux'
// components
import CleanLayout from '../../Components/CleanLayout'
import NotificationItem from '../components/NotificationItem'
import ContentNotLogIn from '../components/ContentNotLogIn'
// actions
import NotificationActions from './NotificationsRedux'
// styles
import styles from '../../assets/Styles/index'
import I18n from '../../I18n/index'

class NotificationsScreen extends Component {
  state = {
    refreshing: false
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.notifications) {
      this.setState({refreshing: false})
    }
  }
  getNotifications = (refreshing) => {
    if (refreshing) {
      this.setState({refreshing: true})
    }
    this.props.getNotifications(0)
  }
  onEndReached = (fetching, page, getNotifications) => {
    if (!fetching) {
      getNotifications(page)
    }
  }

  renderSeparator = () => {
    return <View style={{height: 15}} />
  }
  render () {
    const { refreshing } = this.state
    const {notifications, loggedIn, page, fetching, getNotifications} = this.props
    return (
      <CleanLayout
        fullLoading={fetching}
      >
        <View>
          {
            loggedIn ? <FlatList
              data={notifications}
              style={{marginTop: 15}}
              refreshControl={
                <RefreshControl
                  refreshing={refreshing}
                  onRefresh={() => this.getNotifications('refreshing')}
                />
              }
              keyExtractor={(item, index) => item.id.toString()}
              renderItem={({item}) => <NotificationItem item={item} />}
              ItemSeparatorComponent={this.renderSeparator}
              onEndReachedThreshold={0.2}
              onEndReached={() => { this.onEndReached(fetching, page, getNotifications) }}
            /> : <ContentNotLogIn navigation={() => this.props.navigation.navigate('LoginScreen')} />
          }
        </View>
      </CleanLayout>
    )
  }
}
const mapStateToProps = (state) => ({
  loggedIn: state.auth.loggedIn,
  notifications: state.notifications.list,
  fetching: state.notifications.fetching,
  page: state.notifications.page
})
const mapDispatchToProps = (dispatch) => ({
  getNotifications: (page) => dispatch(NotificationActions.getNotificationRequest(page))
})
NotificationsScreen.navigationOptions = {
  title: I18n.t('Notification')
}
export default connect(mapStateToProps, mapDispatchToProps)(NotificationsScreen)
