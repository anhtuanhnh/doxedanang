import { call, put } from 'redux-saga/effects'
import NotificationActions from './NotificationsRedux'

export function * getNotifications (api, action) {
  // make the call to the api
  const { page } = action
  const response = yield call(api.getNotifications, page)
  if (response.ok) {
    const { data } = response
    // do data conversion here if needed
    yield put(NotificationActions.getNotificationSuccess(data))
  } else {
    yield put(NotificationActions.getNotificationFailure())
  }
}
