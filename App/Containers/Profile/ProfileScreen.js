import React, { Component } from 'react'
import { View, Image } from 'react-native'
// libraries
import Validator from 'validatorjs'
// components
import BaseLayout from '../../Components/BaseLayout'
import Input from '../../Components/Input'
import FullButton from '../../Components/FullButton'
// styles
import styles from '../../assets/Styles/index'
import { Colors, Metrics } from '../../assets/Themes/index'
import img from '../../assets/Images/user.png'
import toast from '../../Components/Toast'
import I18n from '../../I18n/index'
import { connect } from 'react-redux'
import AccountActions from '../Account/AccountRedux'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

class ProfileScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      profile: props.account,
      errors: {},
      disable: false
    }
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.fetching === false) {
      this.setState({disable: false})
    }
  }
  updateProfile = () => {
    this.setState({disable: true})
    const data = {
      name: this.state.profile.name,
      city: this.state.profile.city,
      email: this.state.profile.email
    }
    let rules = {
      email: 'required|email',
      city: 'required',
      name: 'required'
    }
    let validation = new Validator(data, rules)
    if (validation.passes()) {
      this.setState({errors: {}})
      this.props.updateProfile(data)
    } else {
      this.setState({errors: validation.errors.errors})
    }
  };
  render () {
    const { profile, errors, disable } = this.state
    const { fetching } = this.props
    return (
      <KeyboardAwareScrollView>
        <View style={[{paddingHorizontal: 34, backgroundColor: Colors.snow, minHeight: Metrics.screenHeight - 130}]}>
          <View style={{alignItems: 'center', height: 210, marginTop: 34}}>
            <Image source={img} style={{width: 175, height: 175, borderRadius: 150}} />
          </View>
          <Input baseInput styleInput={styles.pldFormInput} value={profile.name} errors={errors.name} iconInput={'ho-va-ten'} press={(name) => this.setState({profile: {...profile, name}})} />
          <Input baseInput styleInput={styles.pldFormInput} editable={false} value={profile.phone} iconInput={'dien-thoai'} />
          <Input baseInput styleInput={styles.pldFormInput} value={profile.email} errors={errors.email} iconInput={'combined-shape'} press={(email) => this.setState({profile: {...profile, email}})} />
          <Input baseInput styleInput={styles.pldFormInput} value={profile.city} errors={errors.city} iconInput={'dia-chi'} press={(city) => this.setState({profile: {...profile, city}})} />
          <Input baseInput styleInput={styles.pldFormInput} value={'123456'} editable={false} iconInput={'mat-khau'} rightPress={(value) => { this.props.navigation.navigate('ChangePasswordScreen') }} iconInputRight={'edit'} secureTextEntry />
        </View>
        <FullButton disable={disable} onPress={this.updateProfile} text={I18n.t('Continue').toUpperCase()} />
      </KeyboardAwareScrollView>
    )
  }
}
ProfileScreen.navigationOptions = {
  title: I18n.t('My profile')
}
const mapStateToProps = (state) => ({
  account: state.account.account,
  fetching: state.account.fetchingProfile
})
const mapDispatchToProps = (dispatch) => ({
  updateProfile: (data) => dispatch(AccountActions.updateProfileRequest(data))
})
export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen)
