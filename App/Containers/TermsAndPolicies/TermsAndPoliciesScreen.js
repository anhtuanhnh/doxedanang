import React, { Component } from 'react'
import {View, Text, Dimensions} from 'react-native'
import {Colors, Metrics} from '../../assets/Themes/index'
import CleanLayout from '../../Components/CleanLayout'
import styles from '../../assets/Styles/index'
import { TabView, TabBar, SceneMap } from 'react-native-tab-view'
import I18n from '../../I18n/index'

class Terms extends Component {
  render () {
    return (
      <View>
        <Text>{I18n.t('Terms of use')}</Text>
      </View>
    )
  }
}
class PoliciesScreen extends Component {
  render () {
    return (
      <View>
        <Text>{I18n.t('Terms of use')}</Text>
      </View>
    )
  }
}
class TermsAndPoliciesScreen extends Component {
  state = {
    index: 0,
    routes: [
      { key: 'Terms', title: I18n.t('Terms of use') },
      { key: 'PoliciesScreen', title: I18n.t('Terms of use') }
    ]
  }
  _handleIndexChange = index => this.setState({ index });
  _renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={styles.indicator}
      style={styles.tabBar}
      tabStyle={styles.tabTermsAndPoliciesScreen}
      labelStyle={styles.labelTab}
      pressColor={Colors.default}

    />
  );
  render () {
    return (
      <CleanLayout>
        <View style={{height: Metrics.screenHeight - 64}}>
          <TabView
            navigationState={this.state}
            renderTabBar={this._renderTabBar}
            renderScene={
              SceneMap({
                Terms: Terms,
                PoliciesScreen: PoliciesScreen
              })
            }
            onIndexChange={this._handleIndexChange}
            initialLayout={{ height: 0, width: Dimensions.get('window').width }}
          />
        </View>
      </CleanLayout>
    )
  }
}
TermsAndPoliciesScreen.navigationOptions = {
  title: I18n.t('Terms and policies')
}
export default TermsAndPoliciesScreen
