import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics } from '../../Themes/'

export const styles = (theme) => StyleSheet.create({
  ...ApplicationStyles.screen,
  xsmall: {
    height: 25,
    width: 25
  },
  small: {
    height: 50,
    width: 50
  },

  medium: {
    height: 75,
    width: 75
  },
  large: {
    height: 150,
    width: 150
  },
  xLarge: {
    height: 500,
    width: 500
  }
})
