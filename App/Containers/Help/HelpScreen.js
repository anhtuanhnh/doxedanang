import React, { Component } from 'react'
import {View, FlatList, Text, TouchableOpacity} from 'react-native'
import {Colors, Metrics} from '../../assets/Themes/index'
import Icon from 'react-native-vector-icons/Ionicons'
import styles from '../../assets/Styles/index'
import CleanLayout from '../../Components/CleanLayout'
import Help from '../../Fixtures/help'
import { createAnimatableComponent } from 'react-native-animatable'
const AnimatableTouchableOpacity = createAnimatableComponent(TouchableOpacity)
import I18n from '../../I18n/index'
class HelpScreen extends Component {
  render () {
    return (
      <CleanLayout>
        <View style={{backgroundColor: Colors.snow, height: Metrics.screenHeight}}>
          {
            Help.map((item, index) => (
              <AnimatableTouchableOpacity
                animation='bounceInRight'
                duration={1500}
                delay={index * 200}
                key={index} style={[styles.rowSpaceAround, { flex: 0, height: 56, paddingHorizontal: 20, borderBottomWidth: 1, borderBottomColor: '#F0F2F6', alignItems: 'center'}]}>
                <Text style={[styles.pldButtonSpecialText, {flex: 2, color: '#4A4A4A'}]}>{item.name}</Text>
                <Icon name={'ios-arrow-round-forward-outline'} size={40} color={Colors.default} />

              </AnimatableTouchableOpacity>
            ))
          }
        </View>
      </CleanLayout>
    )
  }
}

HelpScreen.navigationOptions = ({ navigation }) => ({
  title: I18n.t('Guide')
})
export default HelpScreen
