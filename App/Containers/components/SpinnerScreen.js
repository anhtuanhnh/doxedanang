/* @flow */

import React, { Component } from 'react'
import { View } from 'react-native'
import LottieView from 'lottie-react-native'
import {Images} from '../../Themes'
import {styles} from '../Styles/SpinnerScreenStyle'

export default class SpinnerScreen extends Component {
  componentDidMount () {
    this.spinner.play()
  }

  render () {
    const { size = 'small', type } = this.props
    const themedStyles = styles()

    let source = Images.spinnerDark
    if (type === 'dark') source = Images.spinnerLight
    if (type === 'heart') source = Images.spinnerHeartk
    if (type === 'selector') source = Images.selector

    return (
      <View style={themedStyles[size]}>
        <LottieView
          loop
          style={{}}
          ref={(view) => {
            this.spinner = view
          }}
          source={source}
        />
      </View>
    )
  }
}
