import React from 'react'
import { View, Text, Image } from 'react-native'
import styles from '../../assets/Styles/index'
import img from '../../assets/Images/parkingCar.png'
import NormalButton from '../../Components/NormalButton'
// import Translate from '../../../I18n1/translate';
import I18n from '../../I18n'
const ContentNotLogIn = ({navigation}) => {
  return (
    <View style={styles.boxContent}>
      <Image source={img} style={styles.contentImage} />
      <View style={[styles.p20, { height: 70, marginBottom: 40}]}>
        <Text style={[styles.contentText]}>
          {I18n.t('The current functionality has been limited because you are not logged in, please login to experience better Smart Parking')}
        </Text>
      </View>
      <NormalButton onPress={navigation} text={I18n.t('Sign in now')} />
    </View>
  )
}

export default ContentNotLogIn
