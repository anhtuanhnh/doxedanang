import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import moment from 'moment/moment'
import styles from '../../assets/Styles'
import Icons from '../../Components/AppIcons'
import {Metrics} from '../../assets/Themes'
import I18n from '../../I18n'
const NotificationItem = ({item}) => {
  const created = moment(item.created).format('HH:mm DD/MM/YYYY')
  return (
    <View key={item.id} style={{marginLeft: 15, marginRight: 15, padding: 5}}>
      <View style={[ styles.rowFlexStart, { elevation: 5, flex: 1, backgroundColor: 'white', borderRadius: 3}]}>
        <View style={{width: 80, alignItems: 'center', justifyContent: 'center'}}>
          <Icons name={item.type === 0 ? 'he-thong' : 'do-xe'} style={{}} size={45} color={'#3F8BF4'} />
        </View>
        <View style={{flex: 1, padding: 20, paddingLeft: 0, paddingTop: 10}}>
          <View style={[styles.rowSpaceAround, {flex: 0}]}>
            <Text style={[styles.subCallOutTitle, {flex: 1}]}>{item.type === 0 ? I18n.t('Transaction') : I18n.t('System')}</Text>
            <Text style={[styles.pldItemText, {flex: 1}]}>{created}</Text>
          </View>
          <Text style={{fontSize: 13, lineHeight: 18, textAlign: 'justify'}}>
            {item.message}
          </Text>
        </View>
      </View>
    </View>
  )
}

export default NotificationItem
