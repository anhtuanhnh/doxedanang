import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Alert } from 'react-native'
import styles from '../../assets/Styles/index'
const CustomMarker = ({marker}) => {
  const rotate = '180deg'
  return (
    <View style={styles.markerContainer}>
      <View style={styles.markerBox}>
        <Text style={styles.markerText}>
          {marker.parkingCode}
        </Text>
      </View>
      <View style={[styles.heading, { transform: [{ rotate }] }]}>
        <View style={styles.headingPointer} />
      </View>
    </View>

  )
}
export default CustomMarker
