import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styles from '../../assets/Styles/index'
import img from '../../assets/Images/parkingLocation.jpg'
import Icons from '../../Components/AppIcons'

class ParkingCard extends Component {
  state = {
    heightContent: 124
  }
  onLayout = (e) => {
    this.setState({
      width: e.nativeEvent.layout.width,
      heightContent: e.nativeEvent.layout.height,
      x: e.nativeEvent.layout.x,
      y: e.nativeEvent.layout.y
    })
  }
  render () {
    const {
            marker,
            isHeading = true,
            styleCard,
            styleItem
        } = this.props
    const rotate = '180deg'
    return (
      <View style={[styleCard, styles.callOutContainer]}>
        <View style={[styleItem, styles.callOutBox]}>
          <Image
            style={[styles.callOutImage, {height: this.state.heightContent}]}
            source={img}
                    />
          <View style={styles.callOutContent} onLayout={this.onLayout}>
            <View style={styles.subCallOutContent}>
              <Text numberOfLines={2} style={[styles.subCallOutTitle]}>
                {`${marker.name} - `}
                <Text style={[styles.subCallOutPrice]}>{marker.priceByHour}</Text>{'đ/2h'}
              </Text>
            </View>
            <View style={styles.subCallOutContent}>
              <Icons name={'dia-diem-bai-do'} style={styles.subCallOutIcon} size={11} color={'#4A4A4A'} />
              <Text numberOfLines={2} style={styles.subCallOutTextContent}>
                {marker.address}
              </Text>
            </View>
            <View style={styles.subCallOutContent}>
              <Icons name={'thoi-gian'} style={styles.subCallOutIcon} size={11} color={'#4A4A4A'} />
              <Text style={styles.subCallOutTextContent}>
                {marker.openHour}
              </Text>
            </View>
            <View style={styles.subCallOutContent}>
              <Icons name={'vi-tri-con-trong'} style={styles.subCallOutIcon} size={11} color={'#4A4A4A'} />
              <Text style={styles.subCallOutTextContent}>
                {marker.availableParks}
              </Text>
            </View>
          </View>
        </View>
        {
                    isHeading &&
                    <View style={[styles.headingCallOut, { transform: [{ rotate }] }]}>
                      <View style={styles.headingPointerCallOut} />
                    </View>
                }
      </View>
    )
  }
}

export default ParkingCard
