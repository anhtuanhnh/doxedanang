import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import moment from 'moment/moment'
import styles from '../../assets/Styles'
import Icons from '../../Components/AppIcons'
import imgParking from '../../assets/Images/parkingLocation.jpg'
import I18n from '../../I18n'
const CheckInItem = ({item, checkout, extendCheckIn}) => {
  const nowTime = moment().format('HH:mm')
  const checkinTime = moment(item.checkinTime).format('HH:mm')
  const checkoutTime = moment(item.checkoutime).format('HH:mm DD/MM/YYYY')
    // if (moment(item.checkoutime).valueOf() < moment().valueOf()) {
    //     return null;
    // }
  const timeAgo = moment.utc(moment(checkoutTime, 'HH:mm DD/MM/YYYY').diff(moment(nowTime, 'HH:mm DD/MM/YYYY'))).format('HH:mm')

  return (
    <View style={{padding: 5, marginBottom: 15}}>
      <View style={styles.boxContentCheckIn}>
        <View style={styles.contentCheckInTop}>
          <Image source={imgParking} style={styles.contentLeft} resizeMode={'stretch'} />
          <View style={styles.contentRight}>
            <View style={{}}>
              <Text style={[styles.pldItemText, styles.textBold]}>{item.parkingLocation.name || I18n.t('Park BD01')}</Text>
            </View>
            <View style={[styles.rowFlexStart, { paddingRight: 10}]}>
              <Icons name={'dia-diem-bai-do'} style={styles.contentIcon} size={13} color={'#4A4A4A'} />
              <Text style={{lineHeight: 22}} numberOfLines={2}>{item.parkingLocation.description}</Text>
            </View>
            <View style={[styles.rowFlexStart]}>
              <Icons name={'bien-so-xe'} style={styles.contentIcon} size={11} color={'#4A4A4A'} />
              <Text style={{lineHeight: 22}}>{item.carNumber}</Text>
            </View>
          </View>
        </View>
        <View style={styles.contentCheckInCenter}>
          <View style={[styles.columnFlexStart, { flex: 2}]}>
            <View style={styles.rowFlexStart}>
              <Icons name={'thoi-gian'} style={styles.subCallOutIcon} size={11} color={'#4A4A4A'} />
              <Text>{I18n.t('Parking time')}</Text>
            </View>
            <Text style={[styles.pldItemText, styles.textBold]}>{`${checkinTime} - ${checkoutTime}`}</Text>
          </View>
          <View style={styles.columnFlexEnd}>
            <View style={styles.rowFlexEnd}>
              <Icons name={'thoi-gian'} style={styles.subCallOutIcon} size={11} color={'#4A4A4A'} />
              <Text>{I18n.t('Remain')}</Text>
            </View>
            <Text style={[[styles.pldItemText, styles.textBold], {textAlign: 'right'}]}>{timeAgo}</Text>
          </View>
        </View>
        <View style={[styles.rowSpaceAround, { height: 44}]}>
          <TouchableOpacity style={styles.contentButtonLeft} onPress={() => checkout(item)}>
            <Text style={styles.subCallOutPrice}>
              {I18n.t('Check-out').toUpperCase()}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.contentButtonRight} onPress={() => extendCheckIn(item)}>
            <Text style={styles.pldButtonSpecialText}>
              {I18n.t('Extending time').toUpperCase()}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}

export default CheckInItem
