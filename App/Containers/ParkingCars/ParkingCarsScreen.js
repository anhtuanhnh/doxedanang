import React from 'react'
import { View, Text, Image, FlatList, RefreshControl, Alert } from 'react-native'
import {connect} from 'react-redux'
// libraries
// components
import CleanLayout from '../../Components/CleanLayout'
import NormalButton from '../../Components/NormalButton'
import CheckInItem from '../components/CheckInItem'
import ContentNotLogIn from '../components/ContentNotLogIn'
import toast from '../../Components/Toast'
// actions
// import * as actions from '../../Actions/myCheckIn'
// styles
import mapSearch from '../../assets/Images/mapSearch.png'
import styles from '../../assets/Styles/index'
import ParkingCarsActions from './ParkingCarsRedux'
import I18n from '../../I18n/index'

class ParkingCarsScreen extends React.Component {
  state={
    refreshing: false
  }
  getParkingCars = (refreshing) => {
    this.props.getMyCheckIns()
    if (refreshing) {
      this.setState({refreshing: false})
    }
  }
  extendCheckIn = (info) => {
    const data = {
      id: info.id,
      duration: 2
    }
    Alert.alert(
      I18n.t('SmartParking alerts'),
      `${I18n.t(`Are you extending 2 hours for a car`)} ${info.carNumber}`,
      [
        {text: I18n.t('Cancel'), onPress: () => {}},
        {text: I18n.t('OK'),
          onPress: () => {
            this.props.extendCheckIn(data)
          }}
      ],
      { cancelable: false }
    )
  }
  checkout = (info) => {
    const data = {
      id: info.id
    }
    Alert.alert(
      I18n.t('SmartParking alerts'),
      `${I18n.t(`Do you stop parking for`)} ${info.carNumber} ?`,
      [
        {text: I18n.t('Cancel'), onPress: () => {}},
        {text: I18n.t('OK'),
          onPress: () => {
            this.props.checkOut(data)
          }}
      ],
      { cancelable: false }
    )
  }
  onRefreshing = () => {
    this.setState({refreshing: true})
    this.getParkingCars('refreshing')
  }
  get contentAuth () {
    return (
      <View style={styles.boxContent}>
        <Image source={mapSearch} style={styles.contentImage} />
        <View style={[styles.p20, {height: 70, marginBottom: 40}]}>
          <Text style={styles.contentText}>
            {I18n.t('There are no any check in for your car. Please search and choose one for parking')}
          </Text>
        </View>
        <NormalButton onPress={() => { this.props.navigation.navigate('ParkingSearchScreen') }} text={'Tìm bãi đỗ xe'} />
      </View>
    )
  }
  renderSeparator = () => (<View style={{ height: 15 }} />)
  render () {
    const { myCheckIns, loggedIn, fetching } = this.props
    return (
      <CleanLayout
        fullLoading={fetching}
      >
        {
            loggedIn ? (
              myCheckIns ? <FlatList
                data={myCheckIns}
                style={{margin: 15}}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefreshing}
                  />
                }
                showsVerticalScrollIndicator={false}
                keyExtractor={item => item.id.toString()}
                renderItem={({item}) => <CheckInItem item={item} checkout={this.checkout} extendCheckIn={this.extendCheckIn} />}
              /> : this.contentAuth
            ) : <ContentNotLogIn navigation={() => this.props.navigation.navigate('LoginScreen')} />
          }
      </CleanLayout>
    )
  }
}
const mapStateToProps = (state) => ({
  loggedIn: state.auth.loggedIn,
  myCheckIns: state.parkingCars.myCheckIns,
  fetching: state.parkingCars.fetching
})
const mapDispatchToProps = (dispatch) => ({
  checkOut: (data) => dispatch(ParkingCarsActions.checkOutRequest(data)),
  extendCheckIn: (data) => dispatch(ParkingCarsActions.extendCheckInRequest(data)),
  getMyCheckIns: () => dispatch(ParkingCarsActions.parkingCarsRequest())
})
ParkingCarsScreen.navigationOptions = {
  title: I18n.t('CheckIn')
}

export default connect(mapStateToProps, mapDispatchToProps)(ParkingCarsScreen)
