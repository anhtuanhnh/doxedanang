import { call, put } from 'redux-saga/effects'
import { path } from 'ramda'
import ParkingCarsActions from './ParkingCarsRedux'
import toast from '../../Components/Toast'

export function * getMyCheckIns (api, action) {
  // make the call to the api
  const response = yield call(api.getMyCheckIns)
  if (response.ok) {
    const { data } = response
    // do data conversion here if needed
    yield put(ParkingCarsActions.parkingCarsSuccess(data))
  } else {
    yield put(ParkingCarsActions.parkingCarsFailure())
  }
}
export function * extendCheckIn (api, action) {
  const { data } = action
  // make the call to the api
  const response = yield call(api.extendCheckIn, data)
  console.log(response)
  if (response.ok) {
    // do data conversion here if needed
    toast('Gia hạn thành công')
    yield put(ParkingCarsActions.extendCheckInSuccess())
    yield put(ParkingCarsActions.parkingCarsRequest())
  } else {
    yield put(ParkingCarsActions.extendCheckInFailure())
  }
}
export function * checkOut (api, action) {
  const { data } = action
  // make the call to the api
  const response = yield call(api.checkOut, data)
  console.log(response)

  if (response.ok) {
    // do data conversion here if needed
    toast('Kết thúc đỗ xe thành công')
    yield put(ParkingCarsActions.checkOutSuccess())
    yield put(ParkingCarsActions.parkingCarsRequest())
  } else {
    yield put(ParkingCarsActions.checkOutFailure())
  }
}
