import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  parkingCarsRequest: null,
  parkingCarsSuccess: ['data'],
  parkingCarsFailure: null,

  extendCheckInRequest: ['data'],
  extendCheckInSuccess: null,
  extendCheckInFailure: null,

  checkOutRequest: ['data'],
  checkOutSuccess: null,
  checkOutFailure: null
})

export const ParkingCarsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  myCheckIns: null,
  fetching: null,
  error: null
})

/* ------------- Selectors ------------- */

// export const GithubSelectors = {
//   selectAvatar: state => state.github.avatar
// }

/* ------------- Reducers ------------- */

export const parkingCarsRequest = (state) =>
  state.merge({ fetching: true })

export const parkingCarsSuccess = (state, action) => {
  const { data } = action
  return state.merge({ fetching: false, error: null, myCheckIns: data })
}

export const parkingCarsFailure = (state) =>
  state.merge({ fetching: false, error: true })

// extendCheckIn
export const extendCheckInRequest = (state) =>
  state.merge({ fetching: true, error: null })

export const extendCheckInSuccess = (state) => {
  return state.merge({ fetching: false, error: null })
}

export const extendCheckInFailure = (state) =>
  state.merge({ fetching: false, error: true })

// checkOut
export const checkOutRequest = (state) =>
  state.merge({ fetching: true, error: null})

export const checkOutSuccess = (state) => {
  return state.merge({ fetching: false, error: null })
}

export const checkOutFailure = (state) =>
  state.merge({ fetching: false, error: true })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PARKING_CARS_REQUEST]: parkingCarsRequest,
  [Types.PARKING_CARS_SUCCESS]: parkingCarsSuccess,
  [Types.PARKING_CARS_FAILURE]: parkingCarsFailure,
  [Types.EXTEND_CHECK_IN_REQUEST]: extendCheckInRequest,
  [Types.EXTEND_CHECK_IN_SUCCESS]: extendCheckInSuccess,
  [Types.EXTEND_CHECK_IN_FAILURE]: extendCheckInFailure,

  [Types.CHECK_OUT_REQUEST]: checkOutRequest,
  [Types.CHECK_OUT_SUCCESS]: checkOutSuccess,
  [Types.CHECK_OUT_FAILURE]: checkOutFailure
})
