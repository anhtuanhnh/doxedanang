import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getAccountRequest: null,
  getAccountSuccess: ['data'],
  getAccountFailure: null,

  getWalletRequest: null,
  getWalletSuccess: ['data'],
  getWalletFailure: null,

  updateProfileRequest: ['data'],
  updateProfileSuccess: ['data'],
  updateProfileFailure: null
})

export const AccountTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  account: null,
  fetching: null,
  error: null,
  fetchingWallet: null,
  wallet: null,
  success: null
})

/* ------------- Selectors ------------- */

// export const GithubSelectors = {
//   selectAvatar: state => state.github.avatar
// }

/* ------------- Reducers ------------- */

// request getAccount
export const getAccountRequest = (state) =>
  state.merge({ fetching: true })

// successful getAccount
export const getAccountSuccess = (state, action) => {
  const { data } = action
  return state.merge({ fetching: false, error: null, account: data })
}

// failed to getAccount
export const getAccountFailure = (state) =>
  state.merge({ fetching: false, error: true })

// request getWallet
export const getWalletRequest = (state) =>
  state.merge({ fetchingWallet: true })

// successful getWallet
export const getWalletSuccess = (state, action) => {
  const { data } = action
  return state.merge({ fetchingWallet: false, error: null, wallet: data })
}

// failed to getWallet
export const getWalletFailure = (state) =>
  state.merge({ fetchingWallet: false, error: true })

// request updateProfile
export const updateProfileRequest = (state) =>
  state.merge({ fetchingProfile: true, success: false })

// successful updateProfile
export const updateProfileSuccess = (state, action) => {
  const { data } = action
  return state.merge({ fetchingProfile: false, success: true, error: null, account: data })
}

// failed to updateProfile
export const updateProfileFailure = (state) =>
  state.merge({ fetchingProfile: false, error: true })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_ACCOUNT_REQUEST]: getAccountRequest,
  [Types.GET_ACCOUNT_SUCCESS]: getAccountSuccess,
  [Types.GET_ACCOUNT_FAILURE]: getAccountFailure,
  [Types.GET_WALLET_REQUEST]: getWalletRequest,
  [Types.GET_WALLET_SUCCESS]: getWalletSuccess,
  [Types.GET_WALLET_FAILURE]: getWalletFailure,
  [Types.UPDATE_PROFILE_REQUEST]: updateProfileRequest,
  [Types.UPDATE_PROFILE_SUCCESS]: updateProfileSuccess,
  [Types.UPDATE_PROFILE_FAILURE]: updateProfileFailure
})
