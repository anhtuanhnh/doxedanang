import React, { Component } from 'react'
import { StyleSheet, TouchableOpacity, WebView } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import CleanLayout from '../../Components/CleanLayout'
import { Colors } from '../../assets/Themes'

class WebViewScreen extends Component {
  render () {
    const {uri} = this.props.navigation.state.params
    return (
      <CleanLayout>
        <WebView
          source={{uri: uri}}
          style={{}}
                />
      </CleanLayout>
    )
  }
}

WebViewScreen.navigationOptions = ({navigation}) => {
  const {uri} = navigation.state.params || ''
  const {getWallet} = navigation.state.params
  return ({
    title: `${uri.slice(0, 24)}`,
    headerLeft: (
      <TouchableOpacity onPress={() => { navigation.goBack(null); getWallet() }} style={styles.buttonHeader}>
        <Icon name={'md-arrow-back'} style={{marginRight: 20, marginLeft: 5}} size={24} color={Colors.snow} />
      </TouchableOpacity>
    )
  })
}
export default WebViewScreen

const styles = StyleSheet.create({
  buttonHeader: {
    height: 64,
    width: 80,
    alignItems: 'center',
    justifyContent: 'center'
  }
})
