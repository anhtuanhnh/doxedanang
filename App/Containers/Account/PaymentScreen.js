import React, { Component } from 'react'
import {View, Text, Dimensions, TouchableOpacity, Image} from 'react-native'
import CleanLayout from '../../Components/CleanLayout'
import { TabView, TabBar, SceneMap } from 'react-native-tab-view'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button'
import Modal from 'react-native-modalbox'
import toast from '../../Components/Toast'

import styles from '../../assets/Styles/index'
import {Colors, Metrics} from '../../assets/Themes/index'
import Input from '../../Components/Input'
import img from '../../assets/Images/thanh-cong.png'
import Config from 'react-native-config/index'
import axios from 'axios/index'
import FullButton from '../../Components/FullButton'
import I18n from '../../I18n'
import AccountActions from './AccountRedux'
import { connect } from 'react-redux'
const PAYMENT_URL = 'https://dev-payment.doxedanang.com/api'
const sourceCardList = [{
  label: I18n.t('Smart Parking scratch card'),
  key: 1
},
{
  label: I18n.t('Mobile scratch card'),
  key: 2
}
]
class TheCao extends Component {
  state = {
    serinumber: this.props.serinumber,
    sourceCard: I18n.t('Smart Parking scratch card')
  };

  render () {
    return (
      <View style={{padding: 15}}>
        <View style={[styles.pldForm, { borderRadius: 3, elevation: 5}]}>
          <Input
            label={I18n.t('Type card:').toUpperCase()}
            styleLabel={styles.pldFormLabel}
            styleInput={styles.pldFormInput}
            value={this.state.sourceCard}
            options={sourceCardList}
            selectInput
            press={() => {}} />
          <Input
            label={I18n.t('Code card:').toUpperCase()}
            style={{marginTop: 20}}
            styleLabel={styles.pldFormLabel}
            styleInput={styles.pldFormInput}
            value={this.state.serinumber}
            baseInput
            onSubmitEditing={() => this.props.payment()}
            maxLength={12}
            keyboardType={'number-pad'}
            press={(value) => { this.setState({serinumber: value}); this.props.onChangeValue(value) }} />

        </View>
      </View>
    )
  }
}
class TheNoiDia extends Component {
  render () {
    const firstPrices = [
            { label: '100,000 đ', value: 100000 },
            { label: '200,000 đ', value: 200000 }
    ]
    const secondPrices = [
            { label: '300,000 đ', value: 300000 },
            { label: '400,000 đ', value: 400000 }
    ]
    return (
      <View style={{padding: 20}}>
        <View style={[styles.pldForm, {elevation: 5, borderRadius: 3}]}>
          <Text style={styles.moreText}>{I18n.t('Choose amount of money want to recharge')}</Text>
          <RadioForm
            formHorizontal
            animation
            style={{ marginTop: 20, justifyContent: 'space-around', width: Metrics.screenWidth - 80, height: 30, flexDirection: 'row'}}
                    >
            {firstPrices.map((obj, i) => (
              <RadioButton style={{flex: 1, height: 30, width: (Metrics.screenWidth - 80) / 2}} labelHorizontal key={i} >
                <RadioButtonInput
                  obj={obj}
                  index={i}
                  isSelected={this.props.valuePayment === obj.value}
                  onPress={(value) => { this.props.onChangeValue(value) }}
                  borderWidth={1}
                  buttonInnerColor={this.props.valuePayment === obj.value ? Colors.default : 'white'}
                  buttonOuterColor={'#D6DCE7'}
                  buttonSize={14}
                  buttonOuterSize={22}
                  buttonStyle={{}}
                  buttonWrapStyle={{}}
                                />
                <RadioButtonLabel
                  obj={obj}
                  index={i}
                  labelHorizontal
                  onPress={(value) => { this.setState({value}); this.props.onChangeValue(value) }}
                  labelStyle={{fontSize: 15, fontWeight: '600', lineHeight: 25, color: '#C60707', marginRight: 20}}
                  labelWrapStyle={{}}
                                />
              </RadioButton>
                        ))}
          </RadioForm>
          <RadioForm
            formHorizontal
            animation
            style={{ marginTop: 20, justifyContent: 'space-around', width: Metrics.screenWidth - 80, height: 30, flexDirection: 'row'}}
                    >
            {secondPrices.map((obj, i) => (
              <RadioButton style={{flex: 1, height: 30, width: (Metrics.screenWidth - 80) / 2}} labelHorizontal key={i} >
                <RadioButtonInput
                  obj={obj}
                  index={i}
                  isSelected={this.props.valuePayment === obj.value}
                  onPress={(value) => { this.props.onChangeValue(value) }}
                  borderWidth={1}
                  buttonInnerColor={this.props.valuePayment === obj.value ? Colors.default : 'white'}
                  buttonOuterColor={'#D6DCE7'}
                  buttonSize={14}
                  buttonOuterSize={22}
                  buttonStyle={{}}
                  buttonWrapStyle={{}}
                                />
                <RadioButtonLabel
                  obj={obj}
                  index={i}
                  labelHorizontal
                  onPress={(value) => { this.props.onChangeValue(value) }}
                  labelStyle={{fontSize: 14, fontWeight: '600', lineHeight: 25, color: '#C60707', marginRight: 20}}
                  labelWrapStyle={{}}
                                />
              </RadioButton>
                        ))}
          </RadioForm>
        </View>
      </View>
    )
  }
}
class Visa extends Component {
  render () {
    const firstPrices = [
            {label: '100,000 đ', value: 100000 },
            {label: '200,000 đ', value: 200000 }
    ]
    const secondPrices = [
            {label: '300,000 đ', value: 300000 },
            {label: '400,000 đ', value: 400000 }
    ]
    return (
      <View style={{padding: 20}}>
        <View style={[styles.pldForm, {elevation: 5, borderRadius: 3}]}>
          <Text style={styles.moreText}>{I18n.t('Choose amount of money want to recharge')}</Text>
          <RadioForm
            formHorizontal
            animation
            style={{ marginTop: 20, justifyContent: 'space-around', width: Metrics.screenWidth - 80, height: 30, flexDirection: 'row'}}
                    >
            {firstPrices.map((obj, i) => (
              <RadioButton style={{flex: 1, height: 30, width: (Metrics.screenWidth - 80) / 2}} labelHorizontal key={i} >
                <RadioButtonInput
                  obj={obj}
                  index={i}
                  isSelected={this.props.valuePayment === obj.value}
                  onPress={(value) => { this.props.onChangeValue(value) }}
                  borderWidth={1}
                  buttonInnerColor={this.props.valuePayment === obj.value ? Colors.default : 'white'}
                  buttonOuterColor={'#D6DCE7'}
                  buttonSize={14}
                  buttonOuterSize={22}
                  buttonStyle={{}}
                  buttonWrapStyle={{}}
                                />
                <RadioButtonLabel
                  obj={obj}
                  index={i}
                  labelHorizontal
                  onPress={(value) => { this.props.onChangeValue(value) }}
                  labelStyle={{fontSize: 15, fontWeight: '600', lineHeight: 25, color: '#C60707', marginRight: 20}}
                  labelWrapStyle={{}}
                                />
              </RadioButton>
                        ))}
          </RadioForm>
          <RadioForm
            formHorizontal
            animation
            style={{ marginTop: 20, justifyContent: 'space-around', width: Metrics.screenWidth - 80, height: 30, flexDirection: 'row'}}
                    >
            {secondPrices.map((obj, i) => (
              <RadioButton style={{flex: 1, height: 30, width: (Metrics.screenWidth - 80) / 2}} labelHorizontal key={i} >
                <RadioButtonInput
                  obj={obj}
                  index={i}
                  isSelected={this.props.valuePayment === obj.value}
                  onPress={(value) => { this.props.onChangeValue(value) }}
                  borderWidth={1}
                  buttonInnerColor={this.props.valuePayment === obj.value ? Colors.default : 'white'}
                  buttonOuterColor={'#D6DCE7'}
                  buttonSize={14}
                  buttonOuterSize={22}
                  buttonStyle={{}}
                  buttonWrapStyle={{}}
                                />
                <RadioButtonLabel
                  obj={obj}
                  index={i}
                  labelHorizontal
                  onPress={(value) => { this.props.onChangeValue(value) }}
                  labelStyle={{fontSize: 14, fontWeight: '600', lineHeight: 25, color: '#C60707', marginRight: 20}}
                  labelWrapStyle={{}}
                                />
              </RadioButton>
                        ))}
          </RadioForm>
        </View>
      </View>
    )
  }
}

class PaymentScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      index: 0,
      routes: [
                { key: 'theCao', title: I18n.t('Scratch card') },
                { key: 'theNoiDia', title: I18n.t('Domestic card') },
                { key: 'visa', title: 'Visa/Master' }
      ],
      isOpen: false,
      isDisabled: false,
      swipeToClose: true,
      sliderValue: 0.3,
      disabled: false,
      valuePayment: 100000,
      token: props.token,
      profile: props.account
    }
    this.serinumber = ''
  }

  _handleIndexChange = index => { this.setState({ index }); console.log(index) };
  _renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={styles.indicator}
      style={styles.tabBar}
      tabStyle={styles.tab}
      labelStyle={styles.labelTab}
      pressColor={Colors.default}

        />
    );
  payment = () => {
    const { index } = this.state
    this.setState({disabled: true})
    console.log(index)
    if (index === 0) {
      this.sendPaymentToPup()
    } else if (index === 1) {
      this.sendPaymentOrder()
    } else if (index === 2) {
      this.sendPaymentOrderVisa()
    }
  };
  sendPaymentToPup = () => {
    const { token } = this.state
    const data = {
      serinumber: this.serinumber
    }
    axios.post(`${PAYMENT_URL}/topup/card`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
            .then(res => {
              this.nodeRef.open()
              this.props.getMyWallet()
            })
            .catch(err => {
              toast(err.response.data)
              this.setState({disabled: false})
            })
  };
  sendPaymentOrder = () => {
    const { token, profile, valuePayment } = this.state
    const data = {
      totalAmount: valuePayment,
      currency: 'vnd',
      language: 'vi',
      buyerFullname: `${profile.name}`,
      buyerEmail: `${profile.email}`,
      buyerMobile: `0${profile.phone.slice(3, profile.phone.length)}`,
      buyerAddress: `${profile.city}`
    }
    axios.post(`${PAYMENT_URL}/payment/sendPaymentOrder`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
            .then(res => {
              this.setState({disabled: false})
              this.props.navigation.navigate('WebViewScreen', {uri: res.data.checkoutURL, getWallet: () => { this.props.getMyWallet() }})
            })
            .catch(err => {
              this.setState({disabled: false})
              console.log(err)
            })
  };
  sendPaymentOrderVisa = () => {
    const { token, profile, valuePayment } = this.state
    const data = {
      totalAmount: valuePayment,
      currency: 'vnd',
      language: 'vi',
      buyerFullname: `${profile.name}`,
      buyerEmail: `${profile.email}`,
      buyerMobile: `0${profile.phone.slice(3, profile.phone.length)}`,
      buyerAddress: `${profile.city}`
    }
    axios.post(`${PAYMENT_URL}/payment/sendPaymentOrderVisa`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }

    })
            .then(res => {
              console.log(res)
              this.setState({disabled: false})
              this.props.navigation.navigate('WebViewScreen', {uri: res.data.checkout_url, getWallet: () => { this.props.getMyWallet() }})
            })
            .catch(err => {
              this.setState({disabled: false})
            })
  };
  onChangeValue = (value) => {
    const { index } = this.state
    if (index === 0) {
      this.serinumber = value
    } else {
      this.setState({valuePayment: value})
    }
  };
  render () {
    const { serinumber, disabled, valuePayment} = this.state
    return (
      <CleanLayout>
        <View style={{height: Metrics.screenHeight - 80}}>
          <Modal
            style={[styles.modal, { borderRadius: 3, height: Metrics.screenHeight * 0.5}]}
            ref={n => this.nodeRef = n}
            isOpen={this.state.isOpen}
            swipeToClose
            position={'center'}
                    >
            <View style={{alignItems: 'center', justifyContent: 'flex-end', height: Metrics.screenHeight * 0.209}}>
              <Image source={img} style={{width: Metrics.screenHeight * 0.15, height: Metrics.screenHeight * 0.15}} />
            </View>
            <View style={{alignItems: 'center', height: Metrics.screenHeight * 0.207, paddingHorizontal: 30, justifyContent: 'center'}}>
              <Text style={{fontSize: 17, marginBottom: 8, lineHeight: 20, fontWeight: '600'}}>{I18n.t('Success').toUpperCase()}</Text>
              <Text style={[styles.moreText, {textAlign: 'center'}]}>{I18n.t(`Recharge successful `)}</Text>
            </View>
            <TouchableOpacity onPress={() => { this.nodeRef.close(); this.props.navigation.goBack(null) }} style={{ borderTopColor: '#E7EAF0', borderTopWidth: 1, alignItems: 'center', justifyContent: 'center', position: 'absolute', bottom: 0, height: Metrics.screenHeight * 0.084, left: 0, right: 0}}>
              <Text>{I18n.t('Close')}</Text>
            </TouchableOpacity>
          </Modal>
          <TabView
            navigationState={this.state}
            renderTabBar={this._renderTabBar}
            renderScene={
                            SceneMap({
                              theCao: () => (<TheCao onChangeValue={this.onChangeValue} serinumber={serinumber} payment={this.payment} />),
                              theNoiDia: () => (<TheNoiDia valuePayment={valuePayment} onChangeValue={this.onChangeValue} />),
                              visa: () => (<Visa valuePayment={valuePayment} onChangeValue={this.onChangeValue} />)
                            })
                        }
            onIndexChange={this._handleIndexChange}
            initialLayout={{ height: 0, width: Dimensions.get('window').width }}
                    />
          <FullButton disable={disabled} onPress={this.payment} text={I18n.t('Continue').toUpperCase()} />

          {/* <View style={styles.pldButton}> */}
          {/* <TouchableOpacity disabled={disabled} onPress={this.payment} style={[styles.pldButtonItem, styles.pldButtonSpecial]}> */}
          {/* <Text style={styles.pldButtonSpecialText}> */}
          {/* {'Tiếp tục'.toUpperCase()} */}
          {/* </Text> */}
          {/* </TouchableOpacity> */}
          {/* </View> */}

        </View>

      </CleanLayout>
    )
  }
}
const mapStateToProps = (state) => ({
  token: state.auth.token,
  account: state.account.account
})
const mapDispatchToProps = (dispatch) => ({
  getMyWallet: () => dispatch(AccountActions.getWalletRequest())
})
PaymentScreen.navigationOptions = {
  title: I18n.t('Payment card')
}
export default connect(mapStateToProps, mapDispatchToProps)(PaymentScreen)
