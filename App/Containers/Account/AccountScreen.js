import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Alert, ScrollView, RefreshControl, Image } from 'react-native'
import {connect} from 'react-redux'
// libraries
import Ionicons from 'react-native-vector-icons/Ionicons'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button'
// components
import CleanLayout from '../../Components/CleanLayout'
import {Colors, Metrics} from '../../assets/Themes'
import Icons from '../../Components/AppIcons'
// actions
import AccountActions from './AccountRedux'
import AuthActions from '../Auth/AuthRedux'
// styles
import img from '../../assets/Images/user.png'
import styles from '../../assets/Styles/index'
import I18n from '../../I18n'
import toast from '../../Components/Toast'

const radioProps = [
  { label: 'Việt Nam', value: 'vi' },
  { label: 'English', value: 'en' }
]

class AccountScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      value: 'vi',
      refreshing: false
    }
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.account) {
      this.setState({refreshing: false})
    }
  }
  getAccount = (refreshing) => {
    if (refreshing) {
      this.setState({refreshing: true})
    }
    this.props.getMyAccount()
  };
  getWallet = () => {
    this.props.getMyWallet()
  }
  onRefreshing = () => {
    this.getAccount('refreshing')
    this.getWallet()
  }
  onLogOut = () => {
    Alert.alert(
      I18n.t('SmartParking alerts'),
      I18n.t('Do you want to log out'),
      [
        {text: I18n.t('Cancel'), onPress: () => {}},
        {text: I18n.t('OK'),
          onPress: () => {
            toast(I18n.t('Log out success'))
            this.props.logOut()
          }}
      ],
      { cancelable: false }
    )
  };
  requirePayment = () => {
    const { account } = this.props
    if (account.name && account.email && account.city) {
      this.props.navigation.navigate('PaymentScreen', {profile: account, token: this.props.token, getWallet: () => { this.getWallet(this.props.token) }})
    } else {
      Alert.alert(
        I18n.t('SmartParking alerts'),
        I18n.t('Hãy cập nhật thông tin cá nhân trước khi sử dụng chức năng nạp tiền.'),
        [
          {text: I18n.t('Cancel'), onPress: () => {}},
          {text: I18n.t('OK'), onPress: () => { this.props.navigation.navigate('ProfileScreen', {profile: account, token: this.props.token, getAccount: this.getAccount}) }}
        ],
        { cancelable: false }
      )
    }
  }
  onChangeLanguage = (value) => {
    I18n.locale('en')
    // this.props.setLocale(value)
  }
  render () {
    const { account, loggedIn, wallet } = this.props
    const { refreshing } = this.state
    const money = (wallet && wallet.amount) ? wallet.amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,') : '0'
    return (
      <CleanLayout
      >
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this.onRefreshing}
            />
          }
        >
          <View style={{padding: 15, marginBottom: 57}}>
            {
              (loggedIn && account) &&
              <View style={[styles.rowFlexStart, {padding: 5}]}>
                <Image source={img} style={{width: 75, height: 75, borderRadius: 150}} />
                <View style={[styles.columnFlexStart, {width: Metrics.screenWidth - 125, paddingLeft: 15}]}>
                  <Text style={{fontSize: 20, lineHeight: 30, fontWeight: '600'}}>{account.name || 'Chưa cập nhật'}</Text>
                  <Text style={[styles.moreText, {marginTop: 5}]}>
                    {I18n.t('Balance')} :
                    <Text style={styles.textBold}>
                      {money} VND
                    </Text>
                  </Text>
                  <TouchableOpacity onPress={() => { this.props.navigation.navigate('ProfileScreen', {profile: account, token: this.props.token, getAccount: this.getAccount}) }} style={[styles.rowFlexStart, {marginTop: 10}]}>
                    <Icons name={'edit'} style={{marginRight: 5, paddingTop: 4}} size={14} color={Colors.default} />
                    <Text style={styles.textSpecial}>
                      {I18n.t('Personal information')}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            }
            {
              !loggedIn &&
              <View style={[styles.rowFlexStart, {padding: 5}]}>
                <Image source={img} style={{width: 75, height: 75, borderRadius: 150}} />
                <View style={[styles.columnFlexStart, {width: Metrics.screenWidth - 125, paddingLeft: 15}]}>
                  <Text style={{fontSize: 20, lineHeight: 30, fontWeight: '600'}}>{'Xin chào!'}</Text>
                  <TouchableOpacity onPress={() => { this.props.navigation.navigate('LoginScreen') }}>
                    <Text style={styles.moreText}>
                      {`${I18n.t('You need')} `}
                      <Text style={styles.textSpecial}>
                        {I18n.t('Sign In')}
                      </Text>
                      {` ${I18n.t('the account to experience Smart Parking')}`}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            }
            {
              loggedIn &&
              <View style={{backgroundColor: 'white', borderRadius: 3, marginTop: 30, elevation: 5}}>
                <TouchableOpacity onPress={() => { this.props.navigation.navigate('HistoryScreen', {wallet: money}) }} style={[styles.rowFlexStart, {height: 56, alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#F0F2F6'}]}>
                  <View style={{width: 55, alignItems: 'center'}}>
                    <Icons name={'lich-su-giao-dich'} style={{}} size={20} color={Colors.default} />
                  </View>
                  <Text style={styles.moreText}>
                    {I18n.t('Transaction history')}

                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.rowFlexStart, {height: 56, alignItems: 'center'}]} onPress={this.requirePayment}>
                  <View style={{width: 55, alignItems: 'center'}}>
                    <Icons name={'nap-tien-vao'} style={{}} size={20} color={Colors.default} />
                  </View>
                  <Text style={styles.moreText}>
                    {I18n.t('Recharge')}
                  </Text>
                </TouchableOpacity>
              </View>
            }
            <View style={{backgroundColor: 'white', borderRadius: 3, marginTop: 30, elevation: 5}}>
              <TouchableOpacity onPress={() => { this.props.navigation.navigate('HelpScreen') }} style={[styles.rowFlexStart, {height: 56, alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#F0F2F6'}]}>
                <View style={{width: 55, alignItems: 'center'}}>
                  <Icons name={'ve-chung-toi'} style={{}} size={20} color={Colors.default} />
                </View>
                <Text style={styles.moreText}>
                  {I18n.t('About us')}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => { this.props.navigation.navigate('HelpScreen') }} style={[styles.rowFlexStart, {height: 56, alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#F0F2F6'}]}>
                <View style={{width: 55, alignItems: 'center'}}>
                  <Icons name={'huong-dan'} style={{}} size={20} color={Colors.default} />
                </View>
                <Text style={styles.moreText}>
                  {I18n.t('Guide')}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => { this.props.navigation.navigate('TermsAndPoliciesScreen') }} style={[styles.rowFlexStart, {height: 56, alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#F0F2F6'}]}>
                <View style={{width: 55, alignItems: 'center'}}>
                  <Icons name={'dieu-khoan-cs'} style={{}} size={20} color={Colors.default} />
                </View>
                <Text style={styles.moreText}>
                  {I18n.t('Terms and policies')}
                </Text>
              </TouchableOpacity>
              {
                loggedIn &&
                <TouchableOpacity onPress={this.onLogOut} style={[styles.rowFlexStart, {height: 56, alignItems: 'center'}]}>
                  <View style={{width: 55, alignItems: 'center'}}>
                    <Ionicons name={'md-power'} size={20} color={Colors.default} />
                  </View>
                  <Text style={styles.moreText}>
                    {I18n.t('Logout')}
                  </Text>
                </TouchableOpacity>
              }
            </View>
            <View style={{marginTop: 20, backgroundColor: 'white', paddingHorizontal: 20, elevation: 5, borderRadius: 3}}>
              <View style={[styles.rowFlexStart, { height: 56, alignItems: 'center' }]}>
                <Text style={{flex: 1}}>{I18n.t('Language')}</Text>
                <View style={[{ flex: 2 }]}>
                  <RadioForm
                    formHorizontal
                    animation
                    style={styles.rowSpaceAround}
                  >
                    {radioProps.map((obj, i) => (
                      <RadioButton style={{flex: 1}} labelHorizontal key={i} >
                        <RadioButtonInput
                          obj={obj}
                          index={i}
                          isSelected={obj.value === this.state.value}
                          onPress={(value) => { this.onChangeLanguage(value) }}
                          borderWidth={1}
                          buttonInnerColor={obj.value === this.state.value ? Colors.default : 'white'}
                          buttonOuterColor={'#D6DCE7'}
                          buttonSize={14}
                          buttonOuterSize={22}
                          buttonStyle={{}}
                          buttonWrapStyle={{}}
                        />
                        <RadioButtonLabel
                          obj={obj}
                          index={i}
                          labelHorizontal
                          onPress={(value) => { this.onChangeLanguage(value) }}
                          labelStyle={{fontSize: 14, color: '#4A4A4A', marginRight: 20}}
                          labelWrapStyle={{}}
                        />
                      </RadioButton>
                    ))}
                  </RadioForm>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </CleanLayout>
    )
  }
}

const mapStateToProps = (state) => ({
  loggedIn: state.auth.loggedIn,
  account: state.account.account,
  wallet: state.account.wallet
  // locale: state.i18n.locale
})
const mapDispatchToProps = (dispatch) => ({
  logOut: () => dispatch(AuthActions.logOut()),
  getMyAccount: () => dispatch(AccountActions.getAccountRequest()),
  getMyWallet: () => dispatch(AccountActions.getWalletRequest())
})
AccountScreen.navigationOptions = {
  title: I18n.t('Account')
}
export default connect(mapStateToProps, mapDispatchToProps)(AccountScreen)
