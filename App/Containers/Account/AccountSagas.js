import { call, put } from 'redux-saga/effects'
import AccountActions from './AccountRedux'
import { NavigationActions } from 'react-navigation'
import toast from '../../Components/Toast'

export function * getAccount (api, action) {
  try {
    // make the call to the api
    const response = yield call(api.getAccount)
    console.log(response)
    if (response.ok) {
      const { data } = response
      // do data conversion here if needed
      yield put(AccountActions.getAccountSuccess(data))
    } else {
      yield put(AccountActions.getAccountFailure())
    }
  } catch (e) {
    console.tron.display({
      name: 'getAccount error',
      preview: 'expand',
      value: {
        message: e.message,
        e: e
      }
    })
  }
}
export function * getMyWallet (api, action) {
  try {
    // make the call to the api
    const response = yield call(api.getMyWallet)
    if (response.ok) {
      const { data } = response
      console.log('getMyWallet', response)
      // do data conversion here if needed
      yield put(AccountActions.getWalletSuccess(data))
    } else {
      yield put(AccountActions.getWalletFailure())
    }
  } catch (e) {
    console.tron.display({
      name: 'getMyWallet error',
      preview: 'expand',
      value: {
        message: e.message,
        e: e
      }
    })
  }
}
export function * updateProfile (api, action) {
  try {
    const { data } = action
    // make the call to the api
    const response = yield call(api.updateProfile, data)
    const newData = response.data
    console.log('updateProfile', response)
    if (response.ok) {
      // do data conversion here if needed
      toast('Cập nhật tài khoản thành công')
      yield put(AccountActions.updateProfileSuccess(newData))
      const backAction = NavigationActions.back(null)
      yield put(backAction)
    } else {
      let errorMessage = ''
      if (data.error.response) {
        errorMessage = data.error.response.data.errorMessage
      } else {
        errorMessage = 'Cập nhật tài khoản thất bại, vui lòng thử lại.'
      }
      toast(errorMessage)

      yield put(AccountActions.updateProfileFailure())
    }
  } catch (e) {
    console.tron.display({
      name: 'updateProfile error',
      preview: 'expand',
      value: {
        message: e.message,
        e: e
      }
    })
  }
}
