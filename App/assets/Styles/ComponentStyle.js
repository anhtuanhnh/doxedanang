import { StyleSheet, Platform } from 'react-native'
import { Fonts, Colors, Metrics, ApplicationStyles } from '../Themes/index'
export default StyleSheet.create({
  boxHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 64,
    width: Metrics.screenWidth,
    position: 'relative',
    zIndex: 2
        // marginBottom: Metrics.doubleBaseMargin
  },
  boxTitleHeader: {
    position: 'relative',
    flex: 1,
    width: Metrics.screenWidth,
    alignItems: 'center',
    justifyContent: 'center'
  },
  boxButton: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    top: 0,
    alignItems: 'center',
    justifyContent: 'center',
    width: 60
  },
  boxButtonRight: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    top: 0,
    alignItems: 'center',
    justifyContent: 'center',
    width: 60
  },

  textTitle: {
    color: Colors.snow,
    fontSize: 17,
    lineHeight: 20,
    fontWeight: '600'
  },
  linearGradient: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 15

  },
  button: {
    height: 0.066 * Metrics.screenHeight,
    backgroundColor: Colors.default,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    borderRadius: 3,
    elevation: 3
        // zIndex: -1
  },
  buttonText: {
    fontSize: 18,
        // fontFamily: 'Gill Sans',
    textAlign: 'center',
    color: '#ffffff',
    backgroundColor: 'transparent'
  },
    // custom style box search header
  boxSearchHeader: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingTop: 5,
    paddingLeft: 20,
    flex: 1,
    width: Metrics.screenWidth,
    position: 'relative'
  },
  inputSearch: {
    height: 36,
    width: Metrics.screenWidth - 70,
    paddingLeft: 40,
    borderRadius: 5,
    backgroundColor: '#ffffff',
    opacity: 0.5
  },
  boxIconLeft: {
    position: 'absolute',
    left: 30,
    top: 26
  },
  boxIconRight: {
    position: 'absolute',
    right: 60,
    top: 24
  },
  boxFilterButton: {
    backgroundColor: 'transparent',
    height: 40,
    width: 60,
    justifyContent: 'center',
    alignItems: 'center'
  },
  boxBaseInput: {
    backgroundColor: Colors.snow,
    height: (6.6 * Metrics.screenHeight) / 100,
    position: 'relative',
    marginBottom: Metrics.doubleBaseMargin,
    borderWidth: 1,
    borderColor: '#E7EAF0',
    borderRadius: 3
  },
  labelForm: {

  },
  boxIconInput: {
    height: (6.6 * Metrics.screenHeight) / 100,
    width: (6.6 * Metrics.screenHeight) / 100,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1
  },
  boxIconInputRight: {
    height: (6.6 * Metrics.screenHeight) / 100,
    width: (6.6 * Metrics.screenHeight) / 100,
    right: 0,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
  baseInput: {
    paddingLeft: 10,
    marginLeft: (6.6 * Metrics.screenHeight) / 100,
    zIndex: 1
  },
  hasIconRight: {
    marginRight: (6.6 * Metrics.screenHeight) / 100
  },
  selectInput: {
    borderWidth: 1,
    borderColor: '#E7EAF0',
    borderRadius: 3,
    paddingLeft: 20,
    color: '#2C2C2C',
    fontSize: 15
  },
  iconInput: {

  },
  labelSlider: {
    position: 'absolute',
    right: 0,
    fontSize: 15,
    lineHeight: 18,
    color: '#2C2C2C',
    fontWeight: '600'
  },
  textError: {
    color: 'red',
    marginBottom: 20
  }
})
