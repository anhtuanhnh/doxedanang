import { StyleSheet, Platform } from 'react-native'
import { Fonts, Colors, Metrics, ApplicationStyles } from '../Themes/index'
import TermsAndPoliciesScreen from '../../Containers/TermsAndPolicies/TermsAndPoliciesScreen'

export default StyleSheet.create({
  container: {
    height: Metrics.screenHeight,
    backgroundColor: Colors.background
  },
  mainContainer: {
    padding: Metrics.doubleBaseMargin,
    height: Metrics.screenHeight - 64
  },
  textSpecial: {
    color: Colors.default,
        // fontFamily: Roboto-Medium;
    fontSize: 14,
    lineHeight: 20,
    textDecorationStyle: 'solid',
    textDecorationLine: 'underline'
  },
  rowCenter: {
    flexDirection: 'row',
    justifyContent: 'center',
    flex: 1,
    marginTop: 0.081 * Metrics.screenHeight
  },
  textCenter: {
    textAlign: 'center',
    paddingBottom: 20
  },
    // map style
  mapContainer: {
    position: 'absolute'
  },
  map: {
    ...StyleSheet.absoluteFillObject

  },
    // marker
  markerContainer: {
    width: 42,
    height: 28,
    backgroundColor: 'transparent'
  },
  markerBox: {
    width: 42,
    height: 24,
    borderRadius: 2,
    backgroundColor: '#3F8BF4',
    alignItems: 'center',
    justifyContent: 'center'
  },
  markerText: {
    fontSize: 13,
    lineHeight: 15,
    color: 'white'
  },
  heading: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: 42,
    height: 28,
    alignItems: 'center',
    zIndex: 100,
    backgroundColor: 'transparent'
  },

  headingPointer: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderTopWidth: 0,
    borderRightWidth: 10 * 0.75,
    borderBottomWidth: 7,
    borderLeftWidth: 10 * 0.75,
    borderTopColor: 'red',
    borderRightColor: 'transparent',
    borderBottomColor: '#3F8BF4',
    borderLeftColor: 'transparent'
  },
    // callout
  callOutContainer: {
    backgroundColor: 'white',
    width: Metrics.screenWidth - 40
  },
  callOutBox: {
    flex: 1,
    backgroundColor: 'white',
    borderRadius: 3,
    elevation: 4,
    flexDirection: 'row'
  },
  headingCallOut: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: 0.84 * Metrics.screenWidth,
    height: 0.2 * Metrics.screenHeight,
    alignItems: 'center',
    zIndex: 100,
    backgroundColor: 'transparent'
  },
  headingPointerCallOut: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderTopWidth: 0,
    borderRightWidth: 10 * 0.75,
    borderBottomWidth: 7,
    borderLeftWidth: 10 * 0.75,
    borderTopColor: 'red',
    borderRightColor: 'transparent',
    borderBottomColor: 'white',
    borderLeftColor: 'transparent'
  },
  callOutImage: {
    height: 124,
    width: 104,
    borderTopLeftRadius: 3,
    borderBottomLeftRadius: 3
  },
  callOutContent: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: 10,
    paddingRight: 20,
    borderTopRightRadius: 3,
    borderBottomRightRadius: 3

  },
  subCallOutContent: {
    flexDirection: 'row',
    paddingVertical: 3
  },
  subCallOutIcon: {
    paddingRight: 10,
    paddingTop: 2
  },
  subCallOutTitle: {
    flex: 1,
    fontWeight: '600',
    fontSize: 15,
    lineHeight: 25,
    color: '#2C2C2C'
  },
  subCallOutPrice: {
    fontWeight: '600',
    fontSize: 15,
    lineHeight: 25,
    color: '#C60707'
  },
  subCallOutTextContent: {
    fontSize: 13,
    lineHeight: 16,
    color: '#4A4A4A'
  },
    // parking lot detail
  pldMainContainer: {
    backgroundColor: '#dedede'
  },
  parkingLotDetailBanner: {
    height: 0.32 * Metrics.screenHeight,
    backgroundColor: 'white'
  },
  parkingLotDetailImage: {
    width: '100%',
    height: '100%'
  },
  parkingLotDetailContent: {
    flexDirection: 'column',
    padding: Metrics.doubleBaseMargin,
    backgroundColor: 'white'
  },
  pldItem: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flex: 1
  },
  pb20: {
    paddingBottom: 20
  },
  pldItemText: {
    fontSize: 15,
    lineHeight: 25,
    color: '#4A4A4A'
  },
  textBold: {
    fontWeight: '600'
  },
  pldItemIcon: {
    paddingRight: 10,
    paddingTop: 8
  },
  pldForm: {
    marginTop: 3,
    padding: Metrics.doubleBaseMargin,
    backgroundColor: 'white'
  },
  pldFormLabel: {
    color: '#8F9297',
    fontSize: 13,
    lineHeight: 18,
    fontWeight: '600',
    marginBottom: 10
  },
  pldFormInput: {
    paddingLeft: 20
  },
  formInputSelect: {
    borderWidth: 1,
    borderColor: '#E7EAF0',
    borderRadius: 3,
    paddingLeft: 20,
    paddingRight: 50,
    fontSize: 15,
    fontWeight: '600',
    color: '#2C2C2C'
  },
  pldFormSlider: {
    marginLeft: -15, marginRight: -15
  },
  pldButton: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    height: 50,
    backgroundColor: 'white',
    elevation: 50
  },
  pldButtonItem: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'

  },
  pldButtonSpecial: {
    backgroundColor: Colors.default
  },
  pldButtonSpecialText: {
    fontSize: 14,
    lineHeight: 20,
    color: 'white',
    fontWeight: '600'
  },
    // search
  card: {

  },
  subCard: {

  },
    // checkIn
  boxContent: {
    flex: 1,
    alignItems: 'center',
    padding: 30
  },
  contentImage: {
    width: 200,
    marginTop: 20,
    marginBottom: 30
  },
  p20: {
    padding: 20
  },
  contentText: {
    fontSize: 14,
    lineHeight: 20,
    textAlign: 'center'
  },
  boxContentCheckIn: {
    width: Metrics.screenWidth - 40,
    backgroundColor: 'white',
    borderRadius: 3,
    elevation: 5
  },
  contentCheckInTop: {
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: '#F0F2F6',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  contentLeft: {
    width: 105,
    height: 105,
    borderTopLeftRadius: 3
  },
  contentRight: {
    width: Metrics.screenWidth - 145,
    paddingRight: 30,
    paddingLeft: 10,
    borderTopRightRadius: 3
  },
  rowSpaceAround: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  rowFlexStart: {
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  rowFlexEnd: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  columnFlexEnd: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    flex: 1
  },
  columnFlexStart: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    flex: 1
  },
  pl10: {

  },
  contentIcon: {
    width: 35,
    textAlign: 'center',
    paddingTop: 5
  },
  contentButtonLeft: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F0F2F6',
    borderBottomLeftRadius: 3
  },
  contentButtonRight: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.default,
    borderBottomRightRadius: 3
  },
  contentCheckInCenter: {
    flexDirection: 'row',
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: 'space-around',
    height: 66,
    alignItems: 'center'
  },
  tabBar: {
    backgroundColor: Colors.snow,
    height: 50
  },
  tab: {
    width: Metrics.screenWidth / 3
  },
  tabTermsAndPoliciesScreen: {
    width: Metrics.screenWidth / 2
  },
  labelTab: {
    color: '#8F9297',
    fontWeight: '400'
  },
  indicator: {
    backgroundColor: Colors.default,
    position: 'absolute',
    left: 0,
    bottom: 0,
    right: 0,
    height: 2
  },
  textError: {
    color: 'red',
    marginBottom: 20
  },
  view_container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  hello: {
    fontSize: 20,
    marginBottom: 20
  },
  input: {
    borderWidth: 1,
    borderColor: '#333333',
    fontSize: 12,
    height: 40,
    paddingLeft: 10
  },
  button: {
    borderWidth: 0,
    borderColor: '#333333',
    padding: 10,
    marginTop: 10,
    marginBottom: 10,
    color: '#ffffff',
    backgroundColor: '#fc605c'
  }

})
